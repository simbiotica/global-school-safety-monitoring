<?php

require_once __DIR__.'/../src/app.php';

$app->before(function() use ($app) {
    $app['twig']->addGlobal('env', 'development');
});

$app['debug'] = true;

$app->run();

?>
