define([
  'cartodb',
  'handlebars',
  'text!../../../../templates/common/modal-content.handlebars'
], function(cartodb, Handlebars, tpl) {
  'use strict';

  var ModalContentView = Backbone.View.extend({

    events: {
      'click .close-button': 'closeModalContent'
    },

    initialize: function(options) {
      this.el = options.el;

      Backbone.Events.on('modal:content:hide', this.hideModalContent.bind(this));
      Backbone.Events.on('modal:content:visible', this.showModalContent.bind(this));

      this.render();
    },

    render: function() {
      var $modalElem = $('.module-modal-box');
      var modalContentTpl = Handlebars.compile(tpl);
      
      if(!$modalElem.length) {
        this.$el.prepend(modalContentTpl({}));
        this.$('.module-modal-content').jScrollPane({
          maintainPosition: false,
          autoReinitialise: true,
          animateEase: 'swing',
          horizontalGutter: 0,
          verticalGutter: 0,
          autoReinitialiseDelay: 0,
          initialDelay: 0
        });
      }
    },

    closeModalContent: function() {
      var $modalElem = $('.module-modal-box');
      $modalElem.removeClass('show');
      this.clearContent($modalElem);
      window.GLOBALS.router.navigate('country/' + window.GLOBALS.location.code, true);
    },

    hideModalContent: function() {
      var $modalElem = $('.module-modal-box');
      $modalElem.removeClass('show');
      //this.clearContent($modalElem);
    },

    showModalContent: function() {
      var $modalElem = $('.module-modal-box');
      $modalElem.addClass('show');
    },

    clearContent: function($elem) {
      $elem.one('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',
        function() {
          if(!$elem.hasClass('show')) {
            $elem.find('.module-modal-container').html('');
          }
        });
    }

  });

  return ModalContentView;

});