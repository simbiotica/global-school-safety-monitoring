define([
  'cartodb',
  'handlebars',
  'text!../../../../templates/common/popup.handlebars',
], function(cartodb, Handlebars, tpl) {
  'use strict';

  var PopupView = Backbone.View.extend({

    el: '#popup-box',
    elContent: '#popup-box-text',

    template: Handlebars.compile(tpl),

    events: {
      'click #popup-box-close': 'close'
    },

    initialize: function(options) {
      this.tpl = options.tpl;
      this.$el = $(this.el);
      this.$el.html('');
      this.render();
    },

    render: function() {
      this.$el.html(this.template());
      this.renderContent();
    },

    renderContent: function() {
      if(this.tpl) {
        var contentTpl = Handlebars.compile(this.tpl);
        this.$el.find(this.elContent).html(contentTpl);
      }
    },

    show: function() {
      this.$el.addClass('show');
    },

    close: function() {
      this.$el.removeClass('show');
      Backbone.Events.trigger('close:popup');
    }

  });

  return PopupView;

});