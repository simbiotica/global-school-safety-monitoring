define([
  'cartodb',
  'handlebars',
  'd3'
], function(cartodb, Handlebars, d3) {
  'use strict';

  var ChartsView = Backbone.View.extend({
  
    initialize: function() {
    },

    buildPieChart: function(elem, contentWidth, contentHeight, data) {
      var width = contentWidth,
          height = contentHeight,
          radius = Math.min(width-20, height-20) / 2;

      var arc = d3.svg.arc()
          .outerRadius(radius - 10)
          .innerRadius(0);

      var pie = d3.layout.pie()
          .sort(null)
          .value(function(d) { return d.value; });

      var svg = d3.select(elem).append('svg')
          .attr('width', width)
          .attr('height', height)
        .append('g')
          .attr('transform', 'translate(' + (width) / 2 + ',' + (height) / 2 + ')');

      var g = svg.selectAll('.arc')
          .data(pie(data))
        .enter().append('g')
          .attr('class', 'arc');

      g.append('path')
          .attr('d', arc)
          .style('fill', function(d) { return d.data.color; });

      g.append('text')
       .attr('transform', function(d) {
          var c = arc.centroid(d),
              x = c[0],
              y = c[1],
              h = Math.sqrt(x*x + y*y);
          return 'translate(' + (x/h * radius+5) +  ',' +
             (y/h * radius) +  ')';
        })
        .attr('dy', '.35em')
        .attr('text-anchor', function(d) {
          return (d.endAngle + d.startAngle)/2 > Math.PI ?
              'end' : 'start';
        })
        .text(function(d) { return d.data.value+'%'; });
    },

    buildLegend: function(elem, contentWidth, contentHeight, data) {
      var width = contentWidth,
          height = contentHeight;
      var legendRectSize = 8;
      var legendSpacingH = 8;
      var legendSpacingV = 14;
      var topMargin = 1;

      var svg = d3.select(elem).append('svg')
          .attr('width', width)
          .attr('height', height);

      var legend = svg.selectAll('.legend')
        .data(data)
        .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', function(d, i) {
          var heightRow = legendRectSize + legendSpacingV;
          var vert = i * heightRow + topMargin;
          return 'translate(0,' + vert + ')';
        });

      legend.append('rect')
        .attr('width', legendRectSize)
        .attr('height', legendRectSize)
        .style('fill', function(d){ return d.color; });
        
      legend.append('text')
        .attr('x', legendRectSize + legendSpacingH)
        .attr('y', legendRectSize + (legendSpacingV / legendRectSize) - topMargin)
        .text(function(d) { return d.name; });

    },

    buildBars: function(elem, contentWidth, contentHeight, data, barWidth, barSeparation) {
      var transition = 200;
      var width = contentWidth,
          height = contentHeight;
      var heightPadding = height - 15;

      var svgBars = d3.select(elem).append('svg')
        .attr('class', '')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', 'translate(0,15)');

      var y = d3.scale.linear()
        .range([heightPadding, 0]);

      y.domain([0, d3.max(data.results, function(d) { return d.value; })]);

      var yScale = d3.scale.linear()
       .domain([0, d3.max(data.results, function(d) { return d.value; })])
       .range([heightPadding, 0]);

      var bar = svgBars.selectAll('g')
        .data(data.results)
        .enter().append('g')
        .attr('transform', function(d,i) {
          return 'translate(' + (barWidth+barSeparation)*i + ',0)';
        });

      bar.append('rect')
        .style('fill', function(d) { return d.colour; })
        .attr('class', 'progress-rects')
        .attr('width', barWidth)
        .attr('height', function() {
          return 0;
        })
        .attr('y', function () {
          return heightPadding;
        })
        .transition().duration(transition).ease('linear')
        .attr('height', function(d) {
          return heightPadding - y(d.value);
        })
        .attr('y', function (d) { return  yScale(d.value); });

      svgBars.selectAll('text.bar')
        .data(data.results)
        .enter().append('text')
          .attr('class', 'bar-text')
          .attr('text-anchor', 'middle')
          .attr('x', function(d, i) { return ((barWidth+barSeparation)*i)+7; })
          .attr('y', function(d) { return y(d.value) - 5; })
          .text(function(d) { return d.value; });
    }
  });

  return ChartsView;

});