'use strict';

define([
  'cartodb',
  'handlebars',
  '../../collections/projects',
  '../../collections/schools',
  '../../collections/countries',
  'text!../../../../templates/project.handlebars'
], function(cartodb, Handlebars, ProjectsCollection, SchoolsCollection, CountriesCollection, tpl) {

  var ProjectView = Backbone.View.extend({

    el: '#project',

    events: {
      'click .dashboard-btn-back': 'goBack',
      'click .dashboard-btn-down': 'goDown'
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.projects = new ProjectsCollection();
      this.schools = new SchoolsCollection();
      this.countries = new CountriesCollection();

      Backbone.Events.on('show:world', this.hide, this);
      Backbone.Events.on('show:area', this.hide, this);
      Backbone.Events.on('show:country', this.hide, this);

      Backbone.Events.on('project:show', this.show, this);
      Backbone.Events.on('project:hide', this.hide, this);
      Backbone.Events.on('school:show', this.hide, this);
    },

    render: function() {
      var data = {};

      data.country = this.countries.toJSON()[0];
      data.project = this.projects.toJSON()[0];
      data.schools = this.schools.toJSON();

      if(data.project.id === 10) {
        data.hidden = 'hidden';
        Backbone.Events.trigger('country:per:clear');
        Backbone.Events.trigger('map:clear');
        Backbone.Events.trigger('spin:stop');
      } else {
        data.hidden = '';
      }
      this.$el.html(this.template(data)).fadeIn('fast');

      Backbone.Events.trigger('dashboard:spin:stop');
    },

    show: function(project_id) {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');
      Backbone.Events.trigger('modal:content:hide');

      this.projects.getById(project_id, function(collection) {

        var country_code = collection.toJSON()[0].country_code;

        self.schools.getByCountry(country_code, function() {

          self.countries.getByCode(country_code, function() {
            Backbone.Events.trigger('project:country', country_code);
            self.render();
          });

        });

      });
    },

    hide: function() {
      this.$el.hide().html('');
    },

    goBack: function(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }
      if(window.GLOBALS.location.code !== 'PER') {
        window.history.back();
      } else {
        window.GLOBALS.router.navigate('/country/PER', {trigger: true});
      }
    },

    goDown: function(e) {
      var target = $('#' + $(e.currentTarget).attr('href').split('#')[1]);
      var y = target.offset().top - 88;
      e.preventDefault();
      Backbone.Events.trigger('dashboard:scroll', y);
    }

  });

  return ProjectView;

});
