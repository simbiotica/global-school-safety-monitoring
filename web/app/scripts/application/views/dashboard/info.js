'use strict';

define([
  'cartodb',
  'handlebars',
  '../../models/info',
  '../../collections/projects',
  'text!../../../../templates/info.handlebars'
], function(cartodb, Handlebars, InfoModel, ProjectsCollection, tpl) {

  var InfoView = Backbone.View.extend({

    el: '#info',

    template: Handlebars.compile(tpl),

    options: {
      startAge: 2004,
      endAge: 2013
    },

    initialize: function() {
      this.info = new InfoModel();
      this.projects = new ProjectsCollection();

      Backbone.Events.on('show:world', this.showGlobal, this);
      Backbone.Events.on('show:area', this.showArea, this);
      Backbone.Events.on('show:country', this.hide, this);
      Backbone.Events.on('project:show', this.hide, this);
    },

    render: function() {
      var data = {},
        self = this,
        $timerange,
        $values;

      data.ages = this._getAges(this.options.startAge, this.options.endAge);
      data.timeline_width = (100/(this.options.endAge - (this.options.startAge - 1))) + '%';

      data.info = this.info.toJSON();
      data.projects = this.projects.toJSON();

      this.$el.html(this.template(data)).fadeIn('fast');

      $timerange = $('#timerange');
      $values = this.$el.find('.value');

      $timerange.slider({
        min: this.options.startAge,
        max: this.options.endAge,
        value: window.GLOBALS.year,
        create: function() {
          $('#value' + window.GLOBALS.year).addClass('active');
        },
        change: function(e, ui) {
          $values.removeClass('active');
          $('#value' + ui.value).addClass('active');
          window.GLOBALS.year = ui.value;
          self.reload();
        }
      });

      Backbone.Events.trigger('dashboard:spin:stop');
    },

    showGlobal: function() {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');

      this.info.getGlobal(function() {
        self.projects.getAll(function() {
          self.render();
        });
      });
    },

    showArea: function(area_code) {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');

      this.info.getByArea(area_code, function() {
        self.projects.getByArea(area_code, function() {
          self.render();
        });
      });
    },

    _getAges: function(start, end) {
      var ages = [], i;

      for (i = start; i <= end; i++) {
        ages.push(i);
      }

      return ages;
    },

    hide: function() {
      this.$el.hide().html('');
    },

    reload: function() {
      var location = window.GLOBALS.location;

      switch(location.level) {
      case 0:
        Backbone.Events.trigger('show:world');
        break;
      case 1:
        Backbone.Events.trigger('show:area', location.code);
        break;
      case 2:
        Backbone.Events.trigger('show:country', location.code);
        break;
      }
    }

  });

  return InfoView;

});
