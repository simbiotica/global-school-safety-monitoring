define([
  'cartodb',
  '../../views/common/popup',
  'text!../../../../templates/countries/country-per-info.handlebars'
], function(cartodb, PopupView, CountryPerInfoTpl) {
  'use strict';

  var CountryInfo = Backbone.View.extend({

    initialize: function() {
      this.$el = $(this.el);
      this.$el.html('');
      this.currentCountry = '';

      Backbone.Events.on('show:information', this.show, this);
      Backbone.Events.on('close:popup', this.goBack, this);
    },

    show: function(country) {
      this.currentCountry = country;
      var popup = new PopupView({
        tpl: CountryPerInfoTpl
      });
      popup.show();
    },

    goBack: function() {
      if(this.currentCountry) {
        window.location = '#country/'+this.currentCountry;
      }
    }

  });

  return CountryInfo;

});