'use strict';

define(['cartodb'], function() {

  var DashboardView = Backbone.View.extend({

    el: '.dashboard-content',

    initialize: function() {
      this.$el.jScrollPane({
        maintainPosition: true,
        autoReinitialise: true,
        animateEase: 'swing',
        horizontalGutter: 0,
        verticalGutter: 0
      });

      this.jsp = this.$el.closest('.dashboard-content').data('jsp');

      Backbone.Events.on('dashboard:scroll', this.scrollPane, this);
    },

    scrollPane: function(y) {
      this.jsp.scrollToY(y, true);
    }

  });

  return DashboardView;

});
