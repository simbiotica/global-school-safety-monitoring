define([
  'cartodb',
  'handlebars',
  '../../collections/schools',
  'text!../../../../templates/school.handlebars'
], function(cartodb, Handlebars, SchoolsCollection, tpl) {
  'use strict';

  var SchoolView = Backbone.View.extend({

    el: '#school',

    events: {
      'click .dashboard-btn-back': 'goBack'
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.schools = new SchoolsCollection();

      Backbone.Events.on('school:show', this.show, this);
      Backbone.Events.on('project:show', this.hide, this);
      Backbone.Events.on('show:world', this.hide, this);
      Backbone.Events.on('show:area', this.hide, this);
      Backbone.Events.on('show:country', this.hide, this);
    },

    render: function() {
      var data = this.schools.toJSON()[0];

      Backbone.Events.trigger('school:locate', data);

      this.$el.html(this.template(data)).fadeIn('fast');

      Backbone.Events.trigger('dashboard:spin:stop');
    },

    show: function(id) {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');
      this.schools.getById(id, function() {
        self.render();
      });
    },

    hide: function() {
      this.$el.hide();
    },

    goBack: function(e) {
      if (e.preventDefault) {
        e.preventDefault();
      }
      window.history.back();
    }

  });

  return SchoolView;

});
