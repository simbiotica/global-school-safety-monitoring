define([
  'cartodb',
  'handlebars',
  '../../models/info',
  '../../collections/projects',
  '../../collections/countries',
  'text!../../../../templates/country.handlebars',
  './countries/country-default',
  './countries/country-per/country-per'
], function(
  cartodb,
  Handlebars,
  InfoModel,
  ProjectsCollection,
  CountriesCollection,
  tpl,
  CountryDefaultView,
  CountryPerView) {
  'use strict';

  var CountryView = Backbone.View.extend({

    el: '#country',

    template: Handlebars.compile(tpl),

    countriesViews: {
      'default': CountryDefaultView,
      'PER': CountryPerView
    },

    initialize: function() {
      this.$el = $(this.el);
      this.info = new InfoModel();
      this.projects = new ProjectsCollection();
      this.countries = new CountriesCollection();
      this._layers = [];
      this.setCurrentCountry('default');

      Backbone.Events.on('project:show', this.hide, this);
      Backbone.Events.on('show:country', this.show, this);
      Backbone.Events.on('show:area', this.hide, this);
      Backbone.Events.on('show:world', this.hide, this);
      Backbone.Events.on('school:show', this.hide, this);
      Backbone.Events.on('country:show:content', this.showContent, this);
    },

    render: function() {
      this.currentCountry.render();
    },

    setCurrentCountry: function(country, section, page) {
      var selectedCountry = this.getCurrentCountry(country);

      if(this.currentCountry) {
        this.currentCountry.undelegateEvents();
        this.currentCountry.unBindEvents();
      }
      this.currentCountry = new (this.countriesViews[selectedCountry])({
        info: this.info,
        projects: this.projects,
        countries: this.countries,
        section: section,
        page: page,
        _layers: this._layers
      });
    },

    getCurrentCountry: function(country) {
      var countryCode = 'default';
      window.GLOBALS.currentSection = null;
      if(country === 'PER') {
        countryCode = 'PER';
      }
      return countryCode;
    },

    hide: function() {
      return this.currentCountry.hide();
    },

    show: function(country, section, page) {
      this.removeAllLayers();
      this.setCurrentCountry(country, section, page);
      return this.currentCountry.show(country);
    },

    showContent: function() {
      this.$el.show();
    },

    removeAllLayers: function() {
      var map = window.GLOBALS.map;
      _.each(this._layers, function(layer) {
        map.removeLayer(layer);
      });

      this._layers = [];
    }

  });

  return CountryView;

});