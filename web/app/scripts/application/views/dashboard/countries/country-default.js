define([
  'cartodb',
  'handlebars',
  '../../../models/info',
  '../../../collections/projects',
  '../../../collections/countries',
  'text!../../../../../templates/country.handlebars'
], function(cartodb, Handlebars, InfoModel, ProjectsCollection, CountriesCollection, tpl) {
  'use strict';

  var CountryDefaultView = Backbone.View.extend({

    el: '#country',

    template: Handlebars.compile(tpl),

    events: {
      'click .dashboard-btn-down': 'goDown'
    },

    initialize: function(options) {
      this.$el = $(this.el);
      this.info = options.info;
      this.projects = options.projects;
      this.countries = options.countries;
    },

    render: function() {
      var self = this;

      var data = {
        info: this.info.toJSON(),
        projects: this.projects.toJSON(),
        country: this.countries.toJSON()[0]
      };

      this.$el.html(this.template(data)).fadeIn('fast', function() {
        self.$el.find('.tabs').tabs();
      });

      Backbone.Events.trigger('dashboard:spin:stop');
    },

    show: function(country_code) {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');

      this.projects.getByCountry(country_code, function() {
        self.info.getByCountry(country_code, function() {
          self.countries.getByCode(country_code, function() {
            self.render();
          });
        });
      });
    },

    hide: function() {
      this.$el.hide();
    },

    goDown: function(e) {
      var target = $('#' + $(e.currentTarget).attr('href').split('#')[1]);
      var y = target.offset().top - 88;
      e.preventDefault();
      Backbone.Events.trigger('dashboard:scroll', y);
    },

    unBindEvents: function() {
      
    }

  });

  return CountryDefaultView;

});