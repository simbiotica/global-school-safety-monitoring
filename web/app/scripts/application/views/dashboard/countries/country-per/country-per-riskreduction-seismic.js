define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-riskreduction-seismic.mss'
], function(cartodb,
    Handlebars,
    RiskReductionSeismicCartoCSS) {
  'use strict';

  var PerSeismicRehabilitationView = Backbone.View.extend({
    
    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      this.render();

      $('.seismic-selector').on('click', this.changeGroupLayer.bind(this));
    },

    render: function() {
      Backbone.Events.trigger('modal:content:hide');
      Backbone.Events.trigger('country:per:stats:lima');
      this.renderMap('groupa');
    },

    renderMap: function(group) {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear', this.layers);

      var cartocss = RiskReductionSeismicCartoCSS;

      var tables = {
        groupa: {
          sql: 'escen = \'Good structural behavior\'',
          name: 'Group A: Buildings with good structural performance',
          value: '#56E602'
        },
        groupb: {
          sql: 'escen = \'High damage potential\'',
          name: 'Group B: buildings with high probability of structural damage',
          value: '#1F78B4'
        },
        groupc: {
          sql: 'escen = \'High collapse potential\'',
          name: 'Group C: buildings with high probability of collapse',
          value: '#E62000'
        }
      };

      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM exp_final WHERE '+tables[group].sql
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      var legend = new cdb.geo.ui.Legend({
        type: 'custom',
        data: tables[group]
      });
      $('.legend-box').html(legend.render().el);
      $('.legend-box').addClass('visible');

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;
        self._layers.push(self.cartoDBLayer);

        self.sql.getBounds(options.sublayers[0].sql).on('done', function(bounds) {
          self.map.invalidateSize(false);
          self.map.fitBounds(bounds);
          Backbone.Events.trigger('spin:stop');
        });
      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    changeGroupLayer: function(e) {
      var $currentEl = $(e.currentTarget);
      var $containerMenu = $currentEl.parent();

      if(!$currentEl.hasClass('selected')) {
        $containerMenu.find('li').removeClass('selected');
        $currentEl.toggleClass('selected');
        var group = $currentEl.data('group-level');
        this.renderMap(group);
      }
    },

    removeAllLayers: function() {
      var self = this;
      _.each(this._layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
    }

  });
  return PerSeismicRehabilitationView;

});
