define([
  'cartodb',
  'handlebars',
  'd3',
  '../../../../views/common/charts',
  'text!../../../../../../templates/countries/country-per/country-per-overview-additional.handlebars'
], function(cartodb, Handlebars, d3, ChartsView, tpl) {
  'use strict';

  var PerOverviewAdditional = Backbone.View.extend({

    template: Handlebars.compile(tpl),
    
    events: {
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));

      this.charts = new ChartsView();
      Backbone.Events.trigger('country:per:stats:hidden');
      Backbone.Events.trigger('country:per:clear');
      this.renderGraphs();
    },

    renderGraphs: function() {
      this.graphWhoBuilt();
      this.graphWhenBuilt();
    },

    graphWhoBuilt: function() {
      var elem = '.overview-additional-whobuilt';
      var elemLegend = '.overview-additional-whobuilt-legend';
      var data = [
        {
          'name': 'National government/special project',
          'value': 22,
          'color': '#85B200'
        },
        {
          'name': 'Regional/local governments',
          'value': 31,
          'color': '#4777E0'
        },
        {
          'name': 'Community',
          'value': 41,
          'color': '#BF3944'
        },
        {
          'name': 'Cooperation agencies/NGO´s',
          'value': 4,
          'color': '#FFC500'
        },
        {
          'name': 'Private sector',
          'value': 2,
          'color': '#D4E14E'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 115, data);
    },

    graphWhenBuilt: function() {
      var elem = '.overview-additional-whenbuilt';
      var elemLegend = '.overview-additional-whenbuilt-legend';
      var data = [
        {
          'name': 'Before  1977',
          'value': 8.2,
          'color': '#85B200'
        },
        {
          'name': 'Between 1978 and 1998',
          'value': 34.2,
          'color': '#4777E0'
        },
        {
          'name': 'After 1998',
          'value': 57.2,
          'color': '#BF3944'
        },
        {
          'name': 'Unspecified',
          'value': 0.4,
          'color': '#FFC900'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 155, 115, data);
    }
  });

  return PerOverviewAdditional;

});
