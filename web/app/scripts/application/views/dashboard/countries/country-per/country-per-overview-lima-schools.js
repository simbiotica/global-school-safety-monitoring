define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-lima.mss',
  'text!../../../../../../templates/countries/country-per/infowindows/infowindow-structural-indicators.mustache'
], function(cartodb,
    Handlebars,
    LimaCartoCSS,
    infoWindowStructuralIndicatorsTPL
    ) {
  'use strict';

  var PerOverviewLimaView = Backbone.View.extend({

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      this.render();
    },

    render: function() {
      Backbone.Events.trigger('modal:content:hide');
      Backbone.Events.trigger('country:per:stats:lima');
      this.renderMap();
    },

    renderMap: function() {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear');

      var cartocss = LimaCartoCSS;

      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM all_georeferenced_schools_with_reform_final_2 WHERE departamen = \'LIMA METROPOLITANA\''
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      this.renderLegendLima();

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;

        _.each(masterlayer.layers, function(layer, i) {
          var sublayer = masterlayer.getSubLayer(i);
          sublayer.setInteraction(true);


          if(window.GLOBALS.mapInfoWindow) {
            window.GLOBALS.mapInfoWindow.undelegateEvents();
            window.GLOBALS.mapInfoWindow.remove();
            window.GLOBALS.mapInfoWindow = null;
          }
           
          window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(self.map, sublayer, ['distrito,provincia'], {
            infowindowTemplate: infoWindowStructuralIndicatorsTPL,
            cursorInteraction: true
          });
 
          sublayer.on('featureClick', function(e) {
            e.stopPropagation();
          });
        });

        self._layers.push(masterlayer);

        self.map.invalidateSize(false);
        var latlong = L.latLng(-12.046374, -77.042793);
        self.map.setView(latlong, 11);

        Backbone.Events.trigger('spin:stop');

      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    renderLegendLima: function() {
      var legend = new cdb.geo.ui.Legend({
        type: 'custom',
        data: [
          {name: 'Schools facilities in Lima', value: '#3f98ca'}
        ]
      });
      $('.legend-box').html(legend.render().el);
      $('.legend-box').addClass('visible');
    },

    removeAllLayers: function() {
      Backbone.Events.trigger('map:clear:legend');
      $('.cartodb-infowindow').hide();
      var self = this;
      var layers = this._layers;
      _.each(layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
      layers = [];
    }
  });
  return PerOverviewLimaView;

});