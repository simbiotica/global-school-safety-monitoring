define([
  'cartodb',
  'handlebars',
  'd3',
  '../../../../views/common/charts',
  'text!../../../../../../templates/countries/country-per/country-per-riskconditions-structural.handlebars'
], function(cartodb, Handlebars, d3, ChartsView, tpl) {
  'use strict';

  var PerStructuralTypologiesView = Backbone.View.extend({

    template: Handlebars.compile(tpl),
    
    events: {
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));
      
      this.charts = new ChartsView();
      Backbone.Events.trigger('country:per:stats:hidden');
      Backbone.Events.trigger('country:per:clear');
      this.renderGraphs();
    },

    renderGraphs: function() {
      this.graphStructural();
    },

    graphStructural: function() {
      var elem = '.riskconditions-structural';
      var elemLegend = '.riskconditions-structural-legend';
      var data = [
        {
          'name': 'Reinforced concrete frame',
          'value': 23,
          'color': '#85B200'
        },
        {
          'name': 'Confined/reinforced masonry',
          'value': 25,
          'color': '#BF3944'
        },
        {
          'name': 'Wood',
          'value': 2,
          'color': '#5491F6'
        },
        {
          'name': 'Adobe (mud brick)',
          'value': 25,
          'color': '#DF4AA7'
        },
        {
          'name': 'Unconfined masonry',
          'value': 9,
          'color': '#7E00B2'
        },
        {
          'name': 'Poor construction',
          'value': 14,
          'color': '#FF9D22'
        },
        {
          'name': 'Temporary construction',
          'value': 2,
          'color': '#D4E14E'
        }
      ];

      this.charts.buildPieChart(elem, 350, 380, data);
      this.charts.buildLegend(elemLegend, 300, 160, data);
    }
  });

  return PerStructuralTypologiesView;

});
