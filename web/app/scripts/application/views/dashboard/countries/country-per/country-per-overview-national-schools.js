define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-national-categorized.mss',
  'text!../../../../../../templates/countries/country-per/infowindows/infowindow-structural-indicators.mustache'
], function(cartodb,
    Handlebars,
    NationalCategorizedCartoCSS,
    infoWindowStructuralIndicatorsTPL
    ) {
  'use strict';

  var PerOverviewNationalView = Backbone.View.extend({

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      this.render();
    },

    render: function() {
      Backbone.Events.trigger('modal:content:hide');
      Backbone.Events.trigger('country:per:stats:national');
      this.renderMap();
    },

    renderMap: function() {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear');

      var cartocss = NationalCategorizedCartoCSS;

      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM all_georeferenced_schools_with_reform_final_2'
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      this.renderLegendNational();

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;
        self._layers.push(masterlayer);

        _.each(masterlayer.layers, function(layer, i) {
          var sublayer = masterlayer.getSubLayer(i);

          sublayer.setInteraction(true);

          if(window.GLOBALS.mapInfoWindow) {
            window.GLOBALS.mapInfoWindow.undelegateEvents();
            window.GLOBALS.mapInfoWindow.remove();
            window.GLOBALS.mapInfoWindow = null;
          }

          window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(self.map, sublayer, ['distrito,provincia'], {
            infowindowTemplate: infoWindowStructuralIndicatorsTPL,
            cursorInteraction: true
          });

          sublayer.on('featureClick', function(e) {
            e.stopPropagation();
          });

        });

        self.sql.getBounds(options.sublayers[0].sql).on('done', function(bounds) {
          self.map.invalidateSize(false);
          self.map.fitBounds(bounds);
          Backbone.Events.trigger('spin:stop');
        });

      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    renderLegendNational: function() {
      var legend = new cdb.geo.ui.Legend({
        type: 'custom',
        data: [
          {name: 'Urban', value: '#DF4AA7'},
          {name: 'Rural', value: '#FFC500'}
        ]
      });
      $('.legend-box').html(legend.render().el);
      $('.legend-box').addClass('visible');
    },

    removeAllLayers: function() {
      Backbone.Events.trigger('map:clear:legend');
      $('.cartodb-infowindow').hide();
      var self = this;
      var layers = this._layers;
      _.each(layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
      layers = [];
    }
  });
  return PerOverviewNationalView;

});