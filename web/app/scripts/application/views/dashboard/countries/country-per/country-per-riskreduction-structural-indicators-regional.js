define([
  'cartodb',
  'handlebars',
  '../../../../collections/countries/country-per',
  '../../../../views/common/charts',
  'text!../../../../../../templates/countries/country-per/country-per-riskreduction-structural-regional.handlebars'
], function(cartodb,
  Handlebars,
  CountryPerColletion,
  ChartsView,
  tpl
  ) {
  'use strict';

  var PerStructuralIndicatorsRegionalView = Backbone.View.extend({
    
    events: {
    },

    template: Handlebars.compile(tpl),

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;
      this.countryPerCollection = new CountryPerColletion();
      this.charts = new ChartsView();

      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));

      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('country:per:stats:hidden');
      //this.renderMap();
      this.renderRegionalStats();
    },

    renderRegionalStats: function(){
      var self = this;
      this.countryPerCollection.getByRegions(function(regions){
        self.getRegionalData(regions);
      });
    },

    getRegionalData: function(regions) {
      var self = this;
      var regionsList = regions.toJSON();
      this.countryPerCollection.getByRegionalData(function(regionsData){
        self.renderRegionList(regionsList, regionsData);
      });
    },

    renderRegionList: function(regionsList, regionsData) {
      var self = this;
      var riskList = this.countryPerCollection.riskList();
      var departamentsList = [];
      var combinedItem = [];

      _.map(riskList, function(departament) {
        if(departament.name.search(/Combined intervention/i) === -1){
          departamentsList.push(departament);
        } else {
          combinedItem.push(departament);
        }
      });

      departamentsList.push(combinedItem[0]);

      $('.module-modal-container').html(this.template({regions:regionsList, departamentsList: departamentsList}));

      _.each(regionsList, function(region) {
        var departamenData = regionsData.where({departamen: region.departamen});
        var data = _.map(departamenData, function(model){
          var color = _.where(riskList,{name: model.get('concepto2')});
          if(color.length > 0) {
            model.set('colour',color[0].value);
          }
          return model.toJSON();
        });
        var elem = '.riskreduction-structural-regional-'+region.dep_id;
        self.charts.buildBars(elem, 150, 90, {results:data}, 15, 5);
      });
    }

  });
  return PerStructuralIndicatorsRegionalView;

});