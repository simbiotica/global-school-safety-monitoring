define([
  'cartodb',
  'handlebars',
  'sprintf',
  'skinnytip',
  '../../../../models/info',
  '../../../../collections/projects',
  '../../../../collections/countries',
  '../../../../collections/countries/country-per',
  './country-per-routes',
  '../../../../views/common/modal-content',
  'text!../../../../../../templates/countries/country-per.handlebars',
  'text!../../../../../../templates/countries/country-per/country-per-infostats.handlebars',
  'text!../../../../../../templates/countries/country-per/country-per-infostats-lima.handlebars'
], function(cartodb,
  Handlebars,
  sprintf,
  SkinnyTip,
  InfoModel,
  ProjectsCollection,
  CountriesCollection,
  CountryPerColletion,
  CountryPerRoutes,
  ModalContentView,
  tpl,
  PerNationalStatsInfoTPL,
  PerNationalStatsInfoLimaTPL
  ) {
  'use strict';

  sprintf = sprintf.sprintf;

  var CountryPerView = Backbone.View.extend({

    el: '#country',

    template: Handlebars.compile(tpl),

    cartodbOptions: {
      user_name: 'gpss',
      type: 'cartodb',
      sql: '',
      cartodb_logo: false,
      sublayers: [{}]
    },

    events: {
      'click .collapsible-menu-text': 'toggleCollapsibleMenu',
      'click .country-peru-menu a': 'goToSection'
    },

    initialize: function(options) {
      this.$el = $(this.el);
      this.info = options.info;
      this.projects = options.projects;
      this.countries = options.countries;
      this.section = options.section || 'overview';
      this.page = options.page || 'national-schools';
      this.modalContent = null;
      this.sql = new cartodb.SQL({
        user: this.cartodbOptions.user_name
      });
      this._layers = options._layers;
      this.map = window.GLOBALS.map;
      this.countryPerCollection = new CountryPerColletion();

      window.GLOBALS.currentSection = 'countrySection';
      Backbone.Events.on('country:menu:reset', this.clearMenu);
      Backbone.Events.on('section:show', this.renderSection, this);
      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      Backbone.Events.on('country:per:stats:national', this.getNationalStats.bind(this));
      Backbone.Events.on('country:per:stats:lima', this.getLimaStats.bind(this));
      Backbone.Events.on('country:per:stats:hidden', this.hideStats.bind(this));
    },

    render: function() {
      var self = this;

      var data = {
        info: this.info.toJSON(),
        projects: this.projects.toJSON(),
        country: this.countries.toJSON()[0],
        section: this.section,
        page: this.page
      };

      this.$el.html(this.template(data)).fadeIn('fast', function() {
        self.$el.find('.tabs').tabs();
      });
      SkinnyTip.init();
      Backbone.Events.trigger('section:show', this.section, this.page);
      Backbone.Events.trigger('dashboard:spin:stop');
    },

    renderSection: function(section, page) {
      Backbone.Events.trigger('project:hide');
      Backbone.Events.trigger('country:show:content');

      this.modalContent = new ModalContentView({
        el: '.region-map'
      });

      if(this.countryRoutes) {
        this.countryRoutes.clearViews();
        this.countryRoutes.undelegateEvents();
        this.countryRoutes.remove();
        this.countryRoutes = null;
      }

      if(!this.countryRoutes) {
        this.countryRoutes = new CountryPerRoutes({
          section: section,
          page: page,
          options: {
            map: this.map,
            cartodbOptions: this.cartodbOptions,
            sql: this.sql
          }
        });
      }
    },

    unBindEvents: function() {
      Backbone.Events.off('section:show');
    },

    show: function(country_code) {
      var self = this;

      Backbone.Events.trigger('dashboard:spin:start');

      this.projects.getByCountry(country_code, function() {
        self.countries.getByCode(country_code, function() {
          self.render();
        });
      });

    },

    getNationalStats: function() {
      var self = this;
      var $elem = self.$el.find('.dashboard-info');
      var template = Handlebars.compile(PerNationalStatsInfoTPL);
      $elem.html('');
      $elem.show();
      Backbone.Events.trigger('dashboard:spin:top:start', 55);
      this.countryPerCollection.getByNational(function(res){
        var stats = res.models[0].toJSON();
        Backbone.Events.trigger('dashboard:spin:stop');
        $elem.html(template(stats));
      });
    },

    getLimaStats: function() {
      var self = this;
      var $elem = self.$el.find('.dashboard-info');
      var template = Handlebars.compile(PerNationalStatsInfoLimaTPL);
      $elem.html('');
      $elem.show();
      Backbone.Events.trigger('dashboard:spin:top:start', 55);
      this.countryPerCollection.getByLima(function(res){
        var stats = res.models[0].toJSON();
        Backbone.Events.trigger('dashboard:spin:stop');
        $elem.html(template(stats));
      });
    },

    hideStats: function() {
      var $elem = this.$el.find('.dashboard-info');
      $elem.hide();
    },

    hide: function() {
      this.$el.hide();
    },

    toggleCollapsibleMenu: function(e) {
      var $currentEl = $(e.currentTarget);
      $currentEl.parent().toggleClass('opened');
    },

    clearMenu: function() {
      var $containerEl = $('.country-peru-menu > li');
      $containerEl.removeClass('selected');
    },

    goToSection: function(e) {
      e.preventDefault();

      var $currentTarget = $(e.target);
      var url = $currentTarget.attr('href');
      this.clearMenu();
      $currentTarget.parent('li').addClass('selected');
      if(url) {
        window.location.href = url;
      }
    },

    removeAllLayers: function() {
      Backbone.Events.trigger('map:clear:legend');
      $('.cartodb-infowindow').hide();
      var self = this;
      var layers = this._layers;
      _.each(layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
      layers = [];
    }
  });

  return CountryPerView;

});
