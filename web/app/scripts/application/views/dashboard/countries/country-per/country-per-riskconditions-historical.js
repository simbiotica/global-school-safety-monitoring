define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/country-per-riskconditions-historical.handlebars'
], function(cartodb, Handlebars, tpl) {
  'use strict';

  var PerHistoricalView = Backbone.View.extend({

    template: Handlebars.compile(tpl),
    
    events: {
    },

    initialize: function() {
      Backbone.Events.trigger('country:per:stats:hidden');
      Backbone.Events.trigger('country:per:clear');
      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));
    }
  });

  return PerHistoricalView;

});
