define([
  'cartodb',
  'handlebars',
  'd3',
  '../../../../views/common/charts',
  'text!../../../../../../templates/countries/country-per/country-per-overview-functional.handlebars'
], function(cartodb, Handlebars, d3, ChartsView, tpl) {
  'use strict';

  var PerFunctionalIndicatorsView = Backbone.View.extend({

    template: Handlebars.compile(tpl),
    
    events: {
    },

    initialize: function() {
      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));

      this.charts = new ChartsView();
      Backbone.Events.trigger('country:per:stats:hidden');
      Backbone.Events.trigger('country:per:clear');
      this.renderGraphs();
    },

    renderGraphs: function() {
      this.graphWater();
      this.graphLegal();
      this.graphTelecom();
      this.graphSchool();
      this.graphEnergy();
      this.graphSchoolTotal();
      this.graphSchoolFree();
      this.graphSchoolSport();
    },

    graphWater: function() {
      var elem = '.riskreduction-functional-water';
      var elemLegend = '.riskreduction-functional-water-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 29.3,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 30.0,
          'color': '#4777E0'
        },
        {
          'name': 'No Access',
          'value': 35.6,
          'color': '#BF3944'
        },
        {
          'name': 'Unclassifiable',
          'value': 5.1,
          'color': '#FFC500'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 80, data);
    },

    graphLegal: function() {
      var elem = '.riskreduction-functional-legal';
      var elemLegend = '.riskreduction-functional-legal-legend';
      var data = [
        {
          'name': 'Holds a property title',
          'value': 23.0,
          'color': '#85B200'
        },
        {
          'name': 'Can hold a property title',
          'value': 25.0,
          'color': '#4777E0'
        },
        {
          'name': 'Doesn´t hold a property title',
          'value': 48.0,
          'color': '#BF3944'
        },
        {
          'name': 'Uncertain',
          'value': 0.2,
          'color': '#D4E14E'
        },
        {
          'name': 'Unclassifiable',
          'value': 4.0,
          'color': '#FFC500'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 100, data);
    },

    graphTelecom: function() {
      var elem = '.riskreduction-functional-telecom';
      var elemLegend = '.riskreduction-functional-telecom-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 10.0,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 22.0,
          'color': '#4777E0'
        },
        {
          'name': 'No Access',
          'value': 64.0,
          'color': '#BF3944'
        },
        {
          'name': 'Unclassifiable',
          'value': 4.0,
          'color': '#FFC500'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 80, data);
    },

    graphEnergy: function() {
      var elem = '.riskreduction-functional-energy';
      var elemLegend = '.riskreduction-functional-energy-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 72.0,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 0.5,
          'color': '#4777E0'
        },
        {
          'name': 'No Access',
          'value': 24.0,
          'color': '#BF3944'
        },
        {
          'name': 'Unclassifiable',
          'value': 4.0,
          'color': '#FFC500'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 80, data);
    },

    graphSchool: function() {
      var elem = '.riskreduction-functional-school';
      var elemLegend = '.riskreduction-functional-school-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 69.0,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 18.0,
          'color': '#BF3944'
        },
        {
          'name': 'Unclassifiable',
          'value': 13.0,
          'color': '#FFC500'
        }
      ];

      this.charts.buildPieChart(elem, 300, 240, data);
      this.charts.buildLegend(elemLegend, 300, 70, data);
    },

    graphSchoolTotal: function() {
      var elem = '.riskreduction-functional-total';
      var elemLegend = '.riskreduction-functional-total-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 62.3,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 37.7,
          'color': '#BF3944'
        }
      ];

      this.charts.buildPieChart(elem, 190, 140, data);
      this.charts.buildLegend(elemLegend, 160, 50, data);
    },

    graphSchoolFree: function() {
      var elem = '.riskreduction-functional-free';
      var elemLegend = '.riskreduction-functional-free-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 78.5,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 21.5,
          'color': '#BF3944'
        }
      ];

      this.charts.buildPieChart(elem, 190, 140, data);
      this.charts.buildLegend(elemLegend, 140, 50, data);
    },

    graphSchoolSport: function() {
      var elem = '.riskreduction-functional-sport';
      var elemLegend = '.riskreduction-functional-sport-legend';
      var data = [
        {
          'name': 'Appropriate',
          'value': 46.7,
          'color': '#85B200'
        },
        {
          'name': 'Inappropriate',
          'value': 53.3,
          'color': '#BF3944'
        }
      ];

      this.charts.buildPieChart(elem, 200, 140, data);
      this.charts.buildLegend(elemLegend, 190, 50, data);
    }
  });

  return PerFunctionalIndicatorsView;

});
