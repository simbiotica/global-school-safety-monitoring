define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-annual-avloss.mss'
], function(cartodb, Handlebars, AnnualAverageLossCartoCSS) {
  'use strict';

  var PerAnnualAverageView = Backbone.View.extend({
    
    events: {
    },

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      this.render();
    },

    render: function() {
      Backbone.Events.trigger('country:per:stats:lima');
      Backbone.Events.trigger('modal:content:hide');
      this.renderMap();
    },

    renderMap: function() {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear', this.layers);

      var cartocss = AnnualAverageLossCartoCSS;

      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM images_g_and_j'
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      var legend = new cdb.geo.ui.Legend({
        show_title: true,
        type: 'custom',
        title: 'Ratio of annual average loss<br> to exposure value per school (%)',
        data: [
          { name: '0,1 - 3,0%', value: '#3CA900' },
          { name: '3,0 - 6,0%', value: '#84C701' },
          { name: '6,0 - 9,0%', value: '#FCFC00' },
          { name: '9,0 - 12,0%', value: '#F17F01' },
          { name: '12,0 - 15,4%', value: '#EF2501' }
        ]
      });
      $('.legend-box').html(legend.render().el);
      $('.legend-box').addClass('visible');

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;
        self._layers.push(self.cartoDBLayer);

        self.sql.getBounds(options.sublayers[0].sql).on('done', function(bounds) {
          self.map.invalidateSize(false);
          self.map.fitBounds(bounds);
          Backbone.Events.trigger('spin:stop');
        });
      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    removeAllLayers: function() {
      var self = this;
      _.each(this._layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
    }
  });

  return PerAnnualAverageView;

});