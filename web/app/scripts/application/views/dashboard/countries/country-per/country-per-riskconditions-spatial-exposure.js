define([
  'cartodb',
  'handlebars',
  'sprintf',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-national.mss',
  'text!../../../../../../templates/countries/country-per/infowindows/infowindow-structural-indicators.mustache'
], function(cartodb,
    Handlebars,
    sprintf,
    NationalCartoCSS,
    infoWindowStructuralIndicatorsTPL
    ) {
  'use strict';

  sprintf = sprintf.sprintf;

  var PerRiskconditionsSpatialExposureView = Backbone.View.extend({

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      $('.schools-exposed-selector li').on('click', this.changeSeismicLevel.bind(this));

      this.render(500);
    },

    render: function(level) {
      Backbone.Events.trigger('modal:content:hide');
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('country:per:stats:hidden');
      
      this.renderSchoolsMap();
      this.renderMap(level);
    },

    changeSeismicLevel: function(e) {
      var $currentEl = $(e.currentTarget);
      var $containerMenu = $currentEl.parent();

      if(!$currentEl.hasClass('selected')) {
        $containerMenu.find('li').removeClass('selected');
        $currentEl.toggleClass('selected');
        var level = $currentEl.data('exposed-level');
        this.render(level);
      }
    },

    renderMap: function(level) {
      var self = this;
      Backbone.Events.trigger('hazards:remove');
      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear', this.layers);

      var tables = {
        500: 'yoursadr_500',
        1000: 'yoursadr_1000',
        1500: 'yoursadr_1500'
      };

      var config = {
        'layers': [{
          'type': 'cartodb',
          'options': {
            'sql': sprintf('SELECT * FROM %s', tables[level]),
            'cartocss': '#yoursadr_500{ raster-scaling:near;raster-opacity:0.6;}',
            'cartocss_version': '2.3.0',
            'geom_column': 'the_raster_webmercator',
            'geom_type': 'raster'
          }
        }]
      };
      var url = 'http://gpss.cartodb.com/api/v1/map';
      $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: url,
        data: JSON.stringify(config),
        success: function(response) {
          var layergroup = response;
          var tilesEndpoint = url + '/' + layergroup.layergroupid + '/{z}/{x}/{y}.png';
          var protocol = 'https:' === document.location.protocol ? 'https' : 'http';

          if (layergroup.cdn_url && layergroup.cdn_url[protocol]) {
            var domain = layergroup.cdn_url[protocol];
            if ('http' === protocol) {
              domain = '{s}.' + domain;
            }
            tilesEndpoint = protocol + '://' + domain + '/' + 'gpss/api/v1/map/' + layergroup.layergroupid + '/{z}/{x}/{y}.png';
          }

          if(self.seismicLayer) {
            self.map.removeLayer(self.seismicLayer);
          }

          self.seismicLayer = L.tileLayer(tilesEndpoint, {
            maxZoom: 18
          }).addTo(self.map);
          self._layers.push(self.seismicLayer);

          var legend = new cdb.geo.ui.Legend({
            show_title: true,
            type: 'custom',
            title: 'Peak ground<br> acceleration (gal)',
            data: [
              { name: '0 - 150', value: '#FCFC80' },
              { name: '151 - 300', value: '#F9DD5C' },
              { name: '301 - 450', value: '#F5BA3E' },
              { name: '451 - 600', value: '#F28E24' },
              { name: '601 - 800', value: '#F05713' },
              { name: '801 - 1,000', value: '#EF2502' }
            ]
          });
          $('.legend-box').html(legend.render().el);
          $('.legend-box').addClass('visible');

          Backbone.Events.trigger('spin:stop');
        },
        error: function(){
          Backbone.Events.trigger('spin:stop');
        }
      });
    },

    renderSchoolsMap: function() {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear');

      var cartocss = NationalCartoCSS;

      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM all_georeferenced_schools_with_reform_final_2'
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;
        self._layers.push(masterlayer);

        _.each(masterlayer.layers, function(layer, i) {
          var sublayer = masterlayer.getSubLayer(i);

          sublayer.setInteraction(true);

          if(window.GLOBALS.mapInfoWindow) {
            window.GLOBALS.mapInfoWindow.undelegateEvents();
            window.GLOBALS.mapInfoWindow.remove();
            window.GLOBALS.mapInfoWindow = null;
          }
           
          window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(self.map, sublayer, ['distrito,provincia'], {
            infowindowTemplate: infoWindowStructuralIndicatorsTPL,
            cursorInteraction: true
          });

          sublayer.on('featureClick', function(e) {
            e.stopPropagation();
          });

        });

        self.sql.getBounds(options.sublayers[0].sql).on('done', function(bounds) {
          self.map.invalidateSize(false);
          self.map.fitBounds(bounds);
          Backbone.Events.trigger('spin:stop');
        });

      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    removeAllLayers: function() {
      var self = this;
      _.each(this._layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
    }
  });
  return PerRiskconditionsSpatialExposureView;

});