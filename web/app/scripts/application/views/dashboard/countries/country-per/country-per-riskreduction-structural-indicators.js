define([
  'cartodb',
  'handlebars',
  '../../../../collections/countries/country-per',
  'text!../../../../../../templates/countries/country-per/cartocss/cartocss-per-riskreduction.mss',
  'text!../../../../../../templates/countries/country-per/infowindows/infowindow-structural-indicators-national.mustache'
], function(cartodb,
  Handlebars,
  CountryPerColletion,
  RiskReductionCartoCSS,
  infoWindowStructuralIndicatorsTPL) {
  'use strict';

  var PerStructuralIndicatorsView = Backbone.View.extend({
    
    events: {
    },

    initialize: function(options) {
      this.layers = [];
      this.map = options.map;
      this.cartodbOptions = options.cartodbOptions;
      this.sql = options.sql;
      this.countryPerCollection = new CountryPerColletion();

      Backbone.Events.on('country:per:clear', this.removeAllLayers.bind(this));
      this.render();
    },

    render: function() {
      // this.$el.parents('.module-modal-box').addClass('show');
      // this.$el.html(this.template({}));
      Backbone.Events.trigger('modal:content:hide');
      Backbone.Events.trigger('country:per:stats:hidden');
      this.renderMap();
    },

    renderMap: function() {
      var options = $.extend(true, {}, this.cartodbOptions);
      var self = this;

      Backbone.Events.trigger('spin:start');
      Backbone.Events.trigger('country:per:clear', this.layers);

      var cartocss = RiskReductionCartoCSS;
      options.sublayers = _.map(options.sublayers, function(sublayer) {
        return _.extend(sublayer, {
          interactivity: 'cartodb_id',
          cartocss: cartocss,
          sql: 'SELECT * FROM all_georeferenced_schools_with_reform_final_2'
        });
      });

      this.cartoDBLayer = cartodb.createLayer(this.map, options)
        .addTo(self.map);

      var legend = new cdb.geo.ui.Legend({
        type: 'custom',
        data: self.countryPerCollection.riskList()
      });
      $('.legend-box').html(legend.render().el);
      $('.legend-box').addClass('visible');

      this.cartoDBLayer.on('done', function(masterlayer) {
        self.cartoDBLayer = masterlayer;
        self._layers.push(self.cartoDBLayer);

        _.each(masterlayer.layers, function(layer, i) {
          var sublayer = masterlayer.getSubLayer(i);

          sublayer.setInteraction(true);

          if(window.GLOBALS.mapInfoWindow) {
            window.GLOBALS.mapInfoWindow.undelegateEvents();
            window.GLOBALS.mapInfoWindow.remove();
            window.GLOBALS.mapInfoWindow = null;
          }
          
          window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(self.map, sublayer, ['distrito,provincia,concepto2'], {
            infowindowTemplate: infoWindowStructuralIndicatorsTPL,
            cursorInteraction: true
          });

          sublayer.on('featureClick', function(e) {
            e.stopPropagation();
          });

        });

        self.sql.getBounds(options.sublayers[0].sql).on('done', function(bounds) {
          self.map.invalidateSize(false);
          self.map.fitBounds(bounds);
          Backbone.Events.trigger('spin:stop');
        });

      }).on('error', function() {
        Backbone.Events.trigger('spin:stop');
      });
    },

    removeAllLayers: function() {
      var self = this;
      _.each(this._layers, function(layer) {
        self.map.removeLayer(layer);
      });

      this._layers = [];
    }

  });
  return PerStructuralIndicatorsView;

});
