define([
  'cartodb',

  'views/dashboard/countries/country-per/country-per-overview-national-schools',
  'views/dashboard/countries/country-per/country-per-overview-lima-schools',
  'views/dashboard/countries/country-per/country-per-overview-additional',
  'views/dashboard/countries/country-per/country-per-overview-functional',

  'views/dashboard/countries/country-per/country-per-riskconditions-spatial-exposure',
  'views/dashboard/countries/country-per/country-per-riskconditions-structural',
  'views/dashboard/countries/country-per/country-per-riskconditions-historical',
  'views/dashboard/countries/country-per/country-per-riskreduction-seismic',
  'views/dashboard/countries/country-per/country-per-riskconditions-annual',

  'views/dashboard/countries/country-per/country-per-riskreduction-structural-indicators',
  'views/dashboard/countries/country-per/country-per-riskreduction-structural-indicators-regional',
  'views/dashboard/countries/country-per/country-per-riskreduction-annual-graph',
  'views/dashboard/countries/country-per/country-per-riskreduction-examples'
], function(
  cartodb,

  PerOverviewNationalView,
  PerOverviewLimaView,
  PerOverviewAdditional,
  PerFunctionalIndicatorsView,

  PerRiskconditionsSpatialExposureView,
  PerStructuralTypologiesView,
  PerHistoricalView,
  PerSeismicRehabilitationView,
  PerAnnualAverageView,

  PerStructuralIndicatorsView,
  PerStructuralIndicatorsRegionalView,
  PerRiskReductionAnnualGraphView,
  PerSeismicExampleView
) {
  'use strict';

  var CountryPerRoutes = Backbone.View.extend({

    initialize: function(options) {
      this.section = options.section;
      this.page = options.page;
      this.options = options.options || '';
      this.showSection(this.section, this.page, this.options);
    },

    sectionRoutes: {
      overview: {
        'national-schools': PerOverviewNationalView,
        'lima-schools': PerOverviewLimaView,
        'additional': PerOverviewAdditional,
        'functional': PerFunctionalIndicatorsView,
      },
      riskconditions: {
        'schools-exposed': PerRiskconditionsSpatialExposureView,
        'structural': PerStructuralTypologiesView,
        'historical': PerHistoricalView,
        'seismic-rehabilitation': PerSeismicRehabilitationView,
        'annual-average': PerAnnualAverageView
      },
      riskreduction: {
        'structural-indicators-national': PerStructuralIndicatorsView,
        'structural-indicators-regional': PerStructuralIndicatorsRegionalView,
        'risk-interventions': PerRiskReductionAnnualGraphView,
        'seismic-examples': PerSeismicExampleView
      }
    },

    showSection: function(section, page, options) {
      this.clearViews();
      if(this.sectionRoutes[section] &&
        this.sectionRoutes[section][page] &&
        this.sectionRoutes[section][page] !== 'parent') {
        this.currentSection = new (this.sectionRoutes[section][page])(options);
      } else {
        if(this.sectionRoutes[section] && this.sectionRoutes[section][page] === 'parent') {
          Backbone.Events.trigger('country:per:subpage',page);
        } else {
          //window.location.href = 'map#country/' + window.GLOBALS.location.code;
        }
      }
    },

    clearViews: function() {
      if(this.currentSection) {
        this.currentSection.undelegateEvents();
        this.currentSection.remove();
        this.currentSection = null;
      }
    }

  });

  return CountryPerRoutes;
});