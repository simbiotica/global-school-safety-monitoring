define([
  'cartodb',
  'handlebars',
  'text!../../../../../../templates/countries/country-per/country-per-riskconditions-annual.handlebars'
], function(cartodb, Handlebars, tpl) {
  'use strict';

  var PerRiskReductionAnnualGraphView = Backbone.View.extend({

    template: Handlebars.compile(tpl),
    
    events: {
    },

    initialize: function() {
      Backbone.Events.trigger('country:per:stats:hidden');
      Backbone.Events.trigger('country:per:clear');
      this.render();
    },

    render: function() {
      $('.module-modal-container').parents('.module-modal-box').addClass('show');
      $('.module-modal-container').html(this.template({}));
      Backbone.Events.trigger('spin:stop');
    }
  });

  return PerRiskReductionAnnualGraphView;

});