'use strict';

define([
  'cartodb',
  'd3',
  'handlebars',
  '../../collections/areas',
  'text!../../../../../geojson/areas.geojson',
  'text!../../../../templates/icon.handlebars'
], function(cartodb, d3, Handlebars, AreasCollection, areasGeoData, tpl) {

  var GlobalLayerView = Backbone.View.extend({

    options: {
      borderRadius: 6,
      innerRadius: 16
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.areas = new AreasCollection();
    },

    setLayer: function(area_code, callback) {
      var self = this;
      var scale = d3.scale.log();

      function r(value) {
        if (value > 0) {
          return scale(value) * self.options.innerRadius;
        }
        return 1;
      }

      this.source = window.GLOBALS.source.current.code;

      this.areas.getAllReject(area_code, function(collection) {

        if (self.layer) {
          window.GLOBALS.map.removeLayer(self.layer);
        }

        if (self.geolayer) {
          window.GLOBALS.map.removeLayer(self.geolayer);
        }

        var areas = collection.toGeoJSON();
        var geo_areas = JSON.parse(areasGeoData);

        self.data = areas;

        geo_areas.features = _.reject(geo_areas.features, function(feature) {
          return feature.properties.code === area_code;
        });

        self.geolayer = L.geoJson(geo_areas, {
          style: function() {
            return {
              color: 'transparent'
            };
          },

          onEachFeature: function(feature, layer) {
            layer.on('mouseover', function() {
              layer.setStyle({
                color: '#49A0FF',
                weight: 0
              });
              $('.circle-marker-areas').removeClass('hover');
              $('#layer-'+ feature.properties.code).addClass('hover');
            });

            layer.on('mouseout', function() {
              layer.setStyle({
                color: 'transparent'
              });
              $('.circle-marker-areas').removeClass('hover');
            });

            _.each(areas.features, function(a) {
              if (a.properties.code === feature.properties.code) {
                layer.on('click', function() {
                  window.GLOBALS.router.navigate('area/' + feature.properties.code, {
                    trigger: true
                  });
                });
              }
            });
          }
        });

        self.layer = L.geoJson(areas, {

          onEachFeature: function(feature, layer) {

            var geo = _.find(self.geolayer._layers, function(g) {
              return g.feature.properties.code === feature.properties.code;
            });

            layer.on('mouseover', function() {
              geo.setStyle({
                color: '#49A0FF',
                weight: 0
              });
            });

            layer.on('mouseout', function() {
              geo.setStyle({
                color: 'transparent'
              });
            });

            layer.on('click', function() {
              window.GLOBALS.router.navigate('area/' + feature.properties.code, {
                trigger: true
              });
            });
          },

          pointToLayer: function(feature, latlng) {
            var x = Math.round(r(feature.properties.unknown) + (self.options.borderRadius * 2));
            var y = Math.round(r(feature.properties.unknown) + (self.options.borderRadius * 2));
            var total = feature.properties.unsafe + feature.properties.safe + feature.properties.unknown;

            var schoolIcon = L.divIcon({
              iconSize: [x, y],
              className: 'circle-marker-areas',
              html: self.template({
                total: Number(total).toCommas(),
                unsafe: Number(feature.properties.unsafe).toCommas(),
                safe: Number(feature.properties.safe).toCommas(),
                unknown: Number(feature.properties.unknown).toCommas(),
                official: (self.source === 'official'),
                name: feature.properties.name
              })
            });

            return L.marker(latlng, {
              icon: schoolIcon,
              riseOnHover: true
            });
          }

        });

        if (callback && typeof callback === 'function') {
          callback(collection, self.layer, self.geolayer);
        }
      });

    },

    setMarkerEffects: function() {
      var self = this;

      var minTotal = 1000;
      var innerRadius = self.options.innerRadius;
      var borderRadius = self.options.borderRadius;

      var $markers = $('.circle-marker-areas').find('.circle-marker-data');
      var colors = ['#85b200', '#bf4651', '#adadad'];
      var dataset = _.map(this.data.features, function(data) {
        return [data.properties.safe, data.properties.unsafe, data.properties.unknown];
      });
      var scale = d3.scale.log();
      var counterA = -1;
      var counterB = -1;

      function r(value) {
        if (value > 0) {
          return scale(value) * innerRadius;
        }
        return 1;
      }

      $markers.css({
        'display': 'table',
        'height': (innerRadius * 2) + 'px',
        'width': (innerRadius * 2) + 'px',
        'left': borderRadius + 'px',
        'top': borderRadius + 'px'
      });

      var arc = d3.svg.arc()
        .innerRadius(function(d, i) {
          var total, f;

          if (i === 0) {
            counterA++;
          }
          if (counterA === self.data.features.length) {
            counterA = 0;
          }

          f = self.data.features[counterA].properties;
          total = f.unknown + f.safe + f.unsafe;

          if (total >= minTotal) {
            return r(total) / 2;
          }
          return innerRadius;
        })
        .outerRadius(function(d, i) {
          var total;

          if (i === 0) {
            counterB++;
          }
          if (counterB === self.data.features.length) {
            counterB = 0;
          }

          total = self.data.features[counterB].properties.unknown;

          if (total >= minTotal) {
            return (r(total) / 2) + borderRadius;
          }
          return innerRadius + borderRadius;
        });

      var pie = d3.layout.pie();

      var svg = d3.selectAll('.circle-marker-areas')
        .data(dataset)
        .attr('id', function(d, i) {
          return 'layer-' + self.data.features[i].properties.code;
        })
        .append('svg')
        .style({
          height: function(d, i) {
            var total, f;

            f = self.data.features[i].properties;
            total = f.unknown + f.safe + f.unsafe;

            if (total >= minTotal) {
              return r(total) + (borderRadius * 2) + 'px';
            }
            return (innerRadius + borderRadius) * 2 + 'px';
          },
          width: function(d, i) {
            var f, total;

            f = self.data.features[i].properties;
            total = f.unknown + f.safe + f.unsafe;

            if (total >= minTotal) {
              $($markers[i]).css({
                'width': r(total) + 'px',
                'height': r(total) + 'px',
                'left': borderRadius + 'px',
                'top': borderRadius + 'px'
              });
              return r(total) + (borderRadius * 2) + 'px';
            }
            return (innerRadius + borderRadius) * 2 + 'px';
          }
        })
        .append('g');

      //Set up groups
      svg.selectAll('path')
        .data(pie)
        .enter()
        .append('path')
        .attr('transform', function(d, i, a) {
          var f, total;
          
          f = self.data.features[a].properties;
          total = f.unknown + f.safe + f.unsafe;

          if (total >= minTotal) {
            var t = (r(total) / 2) + borderRadius;
            return 'translate(' + t + ', ' + t + ')';
          }

          return 'translate(' + (innerRadius + borderRadius) + ', ' + (innerRadius + borderRadius) + ')';
        })
        .attr('fill', function(d, i) {
          return colors[i];
        })
        .transition().duration(1000).attrTween('d', function(d, i) {
          var interpolate = d3.interpolate(d.startAngle, d.endAngle);
          return function(t) {
            d.endAngle = interpolate(t);
            return arc(d, i);
          };
        });
    }

  });

  return GlobalLayerView;
});
