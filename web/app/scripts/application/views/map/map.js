define([
  'cartodb',
  'views/map/map-world',
  'views/map/map-area',
  'views/map/map-country'
], function(cartodb, GlobalLayerView, AreaLayerView, CountryLayerView) {
  'use strict';

  var MapView = Backbone.View.extend({

    tagName: 'div',

    id: 'map',

    options: {
      tiles: {
        urlHighZoom: 'http://{s}.tiles.mapbox.com/v3/simbiotica.map-56o5rkkb/{z}/{x}/{y}.png', // Mapbox
        urlNoLabel: 'http://{s}.api.cartocdn.com/base-light-nolabels/{z}/{x}/{y}.png', // CARTODB Tiles No Label
        urlLowZoom: 'http://{s}.api.cartocdn.com/base-light/{z}/{x}/{y}.png', // CARTODB Tiles
        settings: {
          detectRetina: true
        }
      },
      hazardsTiles: {
        layers: [],
        format: 'image/png',
        transparent: true,
        opacity: 0.5
      },
      map: {
        center: [24.206889622398023, 29.355468750000004],
        zoom: 2,
        attributionControl: false,
        zoomControl: false,
        worldCopyJump: true
      }
    },

    initialize: function() {
      this.$body = $('body');
      this._layers = [];

      this.setMap();
      this.setTiles();
      this.setHazardsTiles();

      this.globalView = new GlobalLayerView();
      this.areaView = new AreaLayerView();
      this.countryView = new CountryLayerView();

      Backbone.Events.on('show:world', this.showGlobal, this);
      Backbone.Events.on('show:area', this.showArea, this);
      Backbone.Events.on('show:country', this.showCountry, this);

      Backbone.Events.on('hazards:change', this.setHazardsTiles, this);
      Backbone.Events.on('hazards:remove', this.removeHazardsTiles, this);

      Backbone.Events.on('source:change', this.changeSource, this);
      Backbone.Events.on('school:locate', this.locateSchool, this);

      Backbone.Events.on('project:country', this.showCountry, this);

      Backbone.Events.on('map:clear', this.removeAllLayers, this);
      Backbone.Events.on('map:clear:legend', this.clearLegend, this);

      Backbone.Events.trigger('spin:start');
    },

    setMap: function() {
      if (!this.map) {
        var self = this;

        this.map = L.map(this.id, this.options.map);

        L.control.zoom({
          position: 'topright'
        }).addTo(this.map);

        this.map.on('zoomend', function() {
          if (self.map.getZoom() > 9) {
            self.tiles.setUrl(self.options.tiles.urlHighZoom);
          } else {
            if (window.GLOBALS.location.level === 2) {
              self.tiles.setUrl(self.options.tiles.urlLowZoom);
            } else {
              self.tiles.setUrl(self.options.tiles.urlNoLabel);
            }
          }
        });
      }
      this.setLegend();
      window.GLOBALS.map = this.map;

      return this.map;
    },

    setLegend: function() {
      this.legend = L.control({position: 'bottomleft'});

      this.legend.onAdd = function () {
        var div = L.DomUtil.create('div', 'legend-box');
        return div;
      };
      this.legend.addTo(this.map);
    },

    setTiles: function() {
      if (!this.tiles) {
        this.tiles = L.tileLayer(this.options.tiles.urlNoLabel, this.options.tiles.settings);
        this.tiles.addTo(this.map);
      }
      return this.tiles;
    },

    setHazardsTiles: function(settings) {
      if (!settings) {
        settings = {
          url: '',
          layers: []
        };
      }

      if (!this.HazardsTiles) {
        this.HazardsTiles = L.tileLayer.wms(settings.url, _.extend({
          layers: settings.layers
        }, this.options.hazardsTiles));
        this.HazardsTiles.addTo(this.map);
      } else {
        this.HazardsTiles.setUrl(settings.url)
          .setParams({
            layers: settings.layers,
            transparency: '0.5'
          });
      }

      return this.HazardsTiles;
    },

    showGlobal: function() {
      var self = this;

      Backbone.Events.trigger('spin:start');

      this.removeAllLayers();

      this.globalView.setLayer(null, function(world_data, world_layer, world_geo_layer) {

        self._layers.push(world_layer);
        self._layers.push(world_geo_layer);

        self.map.setView(self.options.map.center, self.options.map.zoom);

        setTimeout(function() {
          self.map.addLayer(world_layer);
          self.map.addLayer(world_geo_layer);
          self.globalView.setMarkerEffects();
        }, 300);

        Backbone.Events.trigger('spin:stop');

      });
    },

    showArea: function(area_code) {
      var self = this;

      Backbone.Events.trigger('spin:start');

      this.removeAllLayers();

      this.globalView.setLayer(area_code, function(world_data, world_layer, world_geo_layer) {

        self._layers.push(world_layer);
        self._layers.push(world_geo_layer);

        self.map.addLayer(world_layer);
        self.map.addLayer(world_geo_layer);

        self.globalView.setMarkerEffects();

      });

      this.areaView.setLayer(null, area_code, function(area_data, area_layer, area_geo_layer) {

        self._layers.push(area_layer);
        self._layers.push(area_geo_layer);

        if (area_code === 'ASE') {
          self.map.setView([-2.7235830833483856, 128.9794921875], 4);
        } else if (area_code === 'ASC') {
          self.map.setView([47.27922900257082, 49.6142578125], 4);
        } else {
          self.map.fitBounds(area_geo_layer.getBounds());
        }

        setTimeout(function() {
          self.map.addLayer(area_layer);
          self.map.addLayer(area_geo_layer);
          self.areaView.setMarkerEffects();
        }, 400);

        Backbone.Events.trigger('spin:stop');

      });

    },

    showCountry: function(country_code, callback) {
      var self = this;

      Backbone.Events.trigger('spin:start');

      this.removeAllLayers();

      this.countryView.setLayer(country_code, function(country_data, country_layer, country_bounds) {

        var area_code = country_data.models[0].get('area_code');

        self.globalView.setLayer(area_code, function(world_data, world_layer, world_geo_layer) {

          self._layers.push(world_layer);
          self._layers.push(world_geo_layer);

          setTimeout(function() {
            self.map.addLayer(world_layer);
            self.map.addLayer(world_geo_layer);
            self.globalView.setMarkerEffects();
          }, 400);

        });

        self.areaView.setLayer(country_code, area_code, function(area_data, area_layer, area_geo_layer) {

          self._layers.push(area_layer);
          self._layers.push(area_geo_layer);

          setTimeout(function() {
            self.map.addLayer(area_layer);
            self.map.addLayer(area_geo_layer);
            self.areaView.setMarkerEffects();
          }, 300);

        });

        self._layers.push(country_layer);

        self.map.fitBounds(country_bounds);

        setTimeout(function() {
          self.map.addLayer(country_layer);
          self.map.invalidateSize();
          if (callback && typeof callback === 'function') {
            callback();
          }
        }, 350);

        Backbone.Events.trigger('spin:stop');

      });
    },

    locateSchool: function(school) {

      var self = this;
      var location = window.GLOBALS.location;
      if (location.level === 2) {
        this.map.setView([school.the_geom.coordinates[1], school.the_geom.coordinates[0]], 17);
      } else {
        this.showCountry(school.country_code, function() {
          self.map.setView([school.the_geom.coordinates[1], school.the_geom.coordinates[0]], 17);
        });
      }

    },

    changeSource: function() {
      var location = window.GLOBALS.location;

      switch(location.level) {
      case 0:
        this.showGlobal();
        break;
      case 1:
        this.showArea(location.code);
        break;
      case 2:
        this.showCountry(location.code);
        break;
      }
    },

    removeAllLayers: function() {
      Backbone.Events.trigger('map:clear:legend');
      var map = this.map;
      _.each(this._layers, function(layer) {
        map.removeLayer(layer);
      });

      this._layers = [];
    },

    removeHazardsTiles: function() {
      if (this.HazardsTiles) {
        this.HazardsTiles.setUrl('')
          .setParams({
            layers: []
          });
      }
    },

    clearLegend: function() {
      $('#map .legend-box').html('');
      $('#map .legend-box').removeClass('visible');
    }

  });

  return MapView;
});
