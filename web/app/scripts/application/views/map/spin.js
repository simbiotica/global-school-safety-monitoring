'use strict';

define(['cartodb'], function() {

  var SpinView = Backbone.View.extend({

    el: '#spinner',

    options: {
      lines: 11, // The number of lines to draw
      length: 3, // The length of each line
      width: 2, // The line thickness
      radius: 4, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#fff', // #rgb or #rrggbb or array of colors
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    },

    initialize: function() {

      this.spin = document.getElementById('spin');

      this.spinner = new Spinner(this.options);

      Backbone.Events.on('spin:start', this.start, this);

      Backbone.Events.on('spin:stop', this.stop, this);

    },

    start: function() {

      this.spinner.spin(this.spin);
      this.$el.fadeIn(300);

    },

    stop: function() {

      this.spinner.stop();
      this.$el.fadeOut(300);

    }

  });

  return SpinView;

});
