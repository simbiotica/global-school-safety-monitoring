'use strict';

define(['cartodb', 'handlebars', 'text!../../../../templates/source.handlebars'], function(cartodb, Handlebars, tpl) {

  var SourceView = Backbone.View.extend({

    el: '#source',

    events: {
      'click a': 'changeMapVisualization',
      'click .icon-helper': 'openOverlayInfo',
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.data = window.GLOBALS.source;
      this.location = window.GLOBALS.location;
      this.render();
    },

    render: function() {
      this.$el.html(this.template(this.data));
    },

    openOverlayInfo: function(e) {
      e.stopPropagation();
      e.preventDefault();
      Backbone.Events.trigger('overlay:show', $('#' + $(e.currentTarget).data('overlay')).html());
    },

    changeMapVisualization: function(e) {
      e.preventDefault();

      var source = $(e.currentTarget).data('source');
      var data = this.data;

      data.current = _.where(data.sources, {
        code: source
      })[0];

      this.render();

      Backbone.Events.trigger('source:change', source);
    }

  });

  return SourceView;

});
