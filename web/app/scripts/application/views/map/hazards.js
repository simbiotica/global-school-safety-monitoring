define([
  'cartodb',
  'handlebars',
  '../../collections/hazards',
  'text!../../../../templates/hazards.handlebars'
], function(cartodb, Handlebars, HazardsCollection, tpl) {
  'use strict';

  var HazardsView = Backbone.View.extend({

    el: '#hazards',

    options: {
      layers: [],
      format: 'image/png',
      transparent: true
    },

    events: {
      'click a': 'setHazardsTiles',
      'click .icon-helper': 'openOverlayInfo',
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.collection = new HazardsCollection();
      this.show();
    },

    render: function() {
      this.$el.html(this.template(this.data));
    },

    show: function() {
      var self = this;

      if (this.collection.length > 0) {
        this.setHazardsTiles();
        this.$el.show();
      } else {
        this.collection.fetch({
          dataType: 'json',
          success: function() {
            self.setHazardsTiles();
            self.$el.show();
          },
          error: function(collection, errors) {
            throw 'Error:' + errors.responseText;
          }
        });
      }
    },

    hide: function() {
      this.$el.hide();
    },

    setHazardsTiles: function(e) {
      var layer;

      if (e && e.preventDefault) {
        e.preventDefault();
        layer = $(e.currentTarget).data('layer');
      }

      if (!layer || layer === 'none') {
        this.data = {
          current: {
            name: 'Hazards',
            layer: 'none',
            slug: 'hazardsOverlay'
          },
          tiles: this.collection.toJSON()
        };

        this.render();

        Backbone.Events.trigger('hazards:remove', this.hazardsTiles);

        return this;
      }

      var HazardInfo = this.collection.where({
        layer: layer
      })[0].toJSON();

      this.data = {
        current: HazardInfo,
        tiles: this.collection.toJSON()
      };

      this.render();

      Backbone.Events.trigger('hazards:change', {
        url: HazardInfo.url,
        layers: [HazardInfo.layer]
      });

      return this.hazardsTiles;
    },

    openOverlayInfo: function(e) {
      e.stopPropagation();
      e.preventDefault();

      var layer = $(e.currentTarget).data('layer');

      var data = this.collection.where({
        layer: layer
      });

      if (data.length > 0) {
        data = data[0].toJSON();
      } else {
        data = null;
      }

      Backbone.Events.trigger('overlay:show', $('#' + $(e.currentTarget).data('overlay')).html());
    }

  });

  return HazardsView;

});
