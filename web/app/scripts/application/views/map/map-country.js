define([
  'cartodb',
  'sprintf',
  '../../collections/countries',
  'text!../../../../../geojson/countries.geojson',
  'text!../../../../templates/infowindow.mustache'
], function(cartodb, sprintf, CountriesCollection, countriesGeoData, tpl) {
  'use strict';

  sprintf = sprintf.sprintf;

  var CountryLayerView = Backbone.View.extend({
    options: {
      user_name: 'gpss',
      type: 'cartodb',
      cartodb_logo: false,
      sublayers: [{}]
    },

    template: tpl,

    initialize: function() {
      this.countries = new CountriesCollection();
      this.sql = new cartodb.SQL({
        user: this.options.user_name
      });
    },

    getCartoCss: function(tableName) {
      var cartocss = [
        '#%1$s {',
        '  marker-fill: #555;',
        '  marker-opacity: 0.3;',
        '  marker-allow-overlap: false;',
        '  marker-placement: point;',
        '  marker-type: ellipse;',
        '  marker-width: 9;',
        '  marker-line-width: .5;',
        '  marker-line-color: #333;',
        '}',
        '#%1$s[safe_code=1] {',
        '  marker-fill:#bf4651;',
        '  marker-opacity: 0.7;',
        '  marker-line-color: #6d1e27;',
        '}',
        '#%1$s[safe_code=2] {',
        '  marker-fill:#85b200;',
        '  marker-opacity: 0.7;',
        '  marker-line-color: #527717;',
        '}',
        '#%1$s[zoom<14] {',
        '  marker-width: 8;',
        '}',
        '#%1$s[zoom<13] {',
        '  marker-width: 8;',
        '}',
        '#%1$s[zoom<12] {',
        '  marker-width: 7;',
        '}',
        '#%1$s[zoom<11] {',
        '  marker-width: 6;',
        '}',
        '#%1$s[zoom<10] {',
        '  marker-width: 4;',
        '}',
        '#%1$s[zoom<8] {',
        '  marker-width: 5;',
        '}'
      ].join('\n');

      return sprintf(cartocss, tableName);
    },

    setLayer: function(country_code, callback) {
      var self = this;
      var source = window.GLOBALS.source.current.code;
      this.currentOptions = $.extend(true, {}, this.options);

      this.currentOptions.sublayers = _.map(this.currentOptions.sublayers, function(sublayer) {

        if (source === 'official') {
          return _.extend(sublayer, {
            interactivity: 'cartodb_id, school_name, level, location, safe_txt, street',
            cartocss: self.getCartoCss('of_schools'),
            sql: sprintf('SELECT * FROM %s WHERE country_code=\'%s\' AND country_code != \'PER\' ORDER BY safe_code DESC', 'of_schools', country_code)
          });
        } else if (source === 'community') {
          return _.extend(sublayer, {
            interactivity: 'cartodb_id, name',
            cartocss: self.getCartoCss('osm_schools'),
            sql: sprintf('SELECT * FROM %s WHERE country_code=\'%s\'', 'osm_schools', country_code)
          });
        }
      });

      this.countries.getByCode(country_code, function(collection) {

        self.setCartoDBLayer(function(cartodb_layer) {

          self.sql.getBounds(self.currentOptions.sublayers[0].sql).on('done', function(bounds) {

            if (callback && typeof callback === 'function') {
              callback(collection, cartodb_layer, bounds);
            }

          });

        });

      });
    },

    setCartoDBLayer: function(callback) {
      var self = this;
      var hovers = [];
      var options = this.currentOptions;
      var map = window.GLOBALS.map;
      var source = window.GLOBALS.source.current.code;
      
      if (this.cartoDBLayer) {
        this.cartoDBLayer.hide();

        _.each(this.cartoDBLayer.layers, function(layer, i) {

          var sublayer = self.cartoDBLayer.getSubLayer(i);

          sublayer.set(options.sublayers[i]);

          if (callback && typeof callback === 'function') {
            callback(self.cartoDBLayer);
          }

        });

        this.cartoDBLayer.show();
      } else {
        this.cartoDBLayer = cartodb.createLayer(map, options)
          .addTo(map);

        this.cartoDBLayer.on('done', function(masterlayer) {
          self.cartoDBLayer = masterlayer;

          _.each(masterlayer.layers, function(layer, i) {
            var sublayer = masterlayer.getSubLayer(i);

            sublayer.setInteraction(true);

            if (source === 'official') {
              if(window.GLOBALS.mapInfoWindow) {
                window.GLOBALS.mapInfoWindow.undelegateEvents();
                window.GLOBALS.mapInfoWindow.remove();
                window.GLOBALS.mapInfoWindow = null;
              }
              window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(map, sublayer, ['cartodb_id', 'school_name', 'level', 'location', 'safe_txt', 'street'], {
                infowindowTemplate: self.template,
                cursorInteraction: false
              });
            } else if (source === 'community') {
              
              if(window.GLOBALS.mapInfoWindow) {
                window.GLOBALS.mapInfoWindow.undelegateEvents();
                window.GLOBALS.mapInfoWindow.remove();
                window.GLOBALS.mapInfoWindow = null;
              }
              window.GLOBALS.mapInfoWindow = cdb.vis.Vis.addInfowindow(map, sublayer, ['cartodb_id', 'name'], {
                infowindowTemplate: self.template,
                cursorInteraction: false
              });
            }

            sublayer.on('featureOver', function(e, latlon, pos, data, layerIndex) {
              hovers[layerIndex] = 1;
              if (_.any(hovers)) {
                document.getElementById(map._container.id).style.cursor = 'pointer';
              }
            });

            sublayer.on('featureOut', function(m, layerIndex) {
              hovers[layerIndex] = 0;
              if (!_.any(hovers)) {
                document.getElementById(map._container.id).style.cursor = 'auto';
              }
            });

            sublayer.on('featureClick', function(e) {
              e.stopPropagation();
            });

            if (callback && typeof callback === 'function') {
              callback(self.cartoDBLayer);
            }

          });
        }).on('error', function(errors) {
          throw errors.responseText;
        });
      }

    }

  });
  return CountryLayerView;

});
