define([
  'cartodb',
  'handlebars',
  '../../collections/areas',
  '../../collections/countries',
  'text!../../../../templates/breadcrumbs.handlebars'
], function(cartodb, Handlebars, AreasCollection, CountriesCollection, tpl) {
  'use strict';

  var BreadcrumbsView = Backbone.View.extend({

    el: '#breadcrumbs',

    template: Handlebars.compile(tpl),

    initialize: function() {

      this.data = {
        current: {}
      };

      this.areas = new AreasCollection();

      this.countries = new CountriesCollection();

      Backbone.Events.on('show:world', this.showGlobal, this);
      Backbone.Events.on('show:area', this.showArea, this);
      Backbone.Events.on('show:country', this.showCountry, this);

      Backbone.Events.on('project:country', this.showCountry, this);

    },

    render: function() {

      this.$el.html(this.template(this.data))
        .fadeIn('fast');
        // .find('.child').jScrollPane({
        //   autoReinitialise: true,
        //   autoReinitialiseDelay: 1000,
        //   animateEase: 'swing',
        //   horizontalGutter: 0,
        //   verticalGutter: 0
        // });

    },

    showGlobal: function() {

      Backbone.Events.trigger('spin:start');

      var self = this;

      this.$el.hide();

      this.data.current = {};

      this.areas.getAll(function() {

        self.data.areas = self.areas.toJSON();

        self.render();

      });

      this.data.countries = this.countries.reset().toJSON();

    },

    showArea: function(area_code) {

      Backbone.Events.trigger('spin:start');

      var self = this;

      this.$el.hide();

      this.data.current = {};

      this.areas.getAll(function() {

        self.data.areas = self.areas.toJSON();

        self.countries.getByArea(area_code, function() {

          self.data.countries = self.countries.toJSON();

          self.areas.getByCode(area_code, function(area) {

            self.data.current.area = area.toJSON()[0];

            self.render();

          });

        });

      });

    },

    showCountry: function(country_code) {

      Backbone.Events.trigger('spin:start');

      var self = this;
      var area_code;

      this.$el.hide();

      this.data.current = {};

      this.areas.getAll(function() {

        self.data.areas = self.areas.toJSON();

        self.countries.getByCode(country_code, function(country) {

          self.data.current.country = country.toJSON()[0];

          area_code = self.data.current.country.area_code;

          self.countries.getByArea(area_code, function() {

            self.data.countries = self.countries.toJSON();

            self.areas.getByCode(area_code, function(area) {

              self.data.current.area = area.toJSON()[0];

              self.render();

            });

          });

        });

      });
    }

  });

  return BreadcrumbsView;

});
