'use strict';

define(['cartodb', 'handlebars', 'text!../../../templates/overlay.handlebars'], function(cartodb, Handlebars, tpl) {

  var OverlayView = Backbone.View.extend({

    el: '#overlay',

    events: {
      'click #overlayCloseBtn': 'hide',
      'mouseout .overlay-content': 'onMouseOut',
      'mouseover .overlay-content': 'onMouseOver'
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      var self = this;

      this.data = {};

      Backbone.Events.on('overlay:show', this.show, this);

      $('a[data-overlay]').on('click', function(e) {
        self.openWithData(e);
      });
    },

    render: function() {
      this.$el.html(this.template({content: this.data})).fadeIn('fast');
    },

    show: function(data) {
      if (data) {
        this.data = data;
      } else {
        this.data = '';
      }

      this.render();
    },

    hide: function() {
      this.$el.fadeOut('fast');
    },

    onMouseOut: function() {
      var self = this;

      this.$el.on('click', function() {
        self.hide();
      });
    },

    onMouseOver: function() {
      var self = this;

      this.$el.off('click');
      $('#overlayCloseBtn').on('click', function(e) {
        e.preventDefault();
        self.hide();
      });
    },

    openWithData: function(e) {
      e.preventDefault();

      Backbone.Events.trigger('overlay:show', $('#' + $(e.currentTarget).data('overlay')).html());
    }

  });

  return OverlayView;

});
