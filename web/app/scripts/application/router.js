define([
  'views/dashboard/dashboard',
  'views/dashboard/info',
  'views/dashboard/project',
  'views/dashboard/school',
  'views/dashboard/country',
  'views/dashboard/country-info',
  'views/dashboard/spin',
  'views/map/map',
  'views/map/spin',
  'views/map/hazards',
  'views/map/breadcrumbs',
  'views/map/source',
  'views/overlay'
], function(
  DashboardView,
  InfoView,
  ProjectView,
  SchoolView,
  CountryView,
  CountryInfoView,
  DashboardSpinView,
  MapView,
  SpinView,
  HazardsView,
  BreadcrumbsView,
  SourceView,
  OverlayView
) {
  'use strict';

  var Router, app;

  app = {};
  app.dashboard = new DashboardView();
  app.info = new InfoView();
  app.project = new ProjectView();
  app.school = new SchoolView();
  app.country = new CountryView();
  app.countryInfo = new CountryInfoView();
  app.map = new MapView();
  app.spin = new SpinView();
  app.hazards = new HazardsView();
  app.breadcrumbs = new BreadcrumbsView();
  app.source = new SourceView();
  app.overlay = new OverlayView();
  app.dashSpin = new DashboardSpinView();

  Router = Backbone.Router.extend({

    routes: {
      '': 'index',
      'global': 'global',
      'area/:id': 'area',
      'country/:id': 'country',
      'country/:id/:section/:page': 'countrySection',
      'project/:id': 'project',
      'school/:id': 'school',
      'info/:id': 'information'
    },

    initialize: function() {
      Backbone.history.start();
    },

    index: function() {
      this.navigate('global', {
        trigger: true
      });
    },

    global: function() {
      window.GLOBALS.location = {
        level: 0,
        code: 'global'
      };
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('show:world'); // show world
    },

    area: function(id) {
      window.GLOBALS.location = {
        level: 1,
        code: id
      };
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('show:area', id); // show area
    },

    country: function(id, section, page) {
      window.GLOBALS.location = {
        level: 2,
        code: id
      };

      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('show:country', id, section, page); // show country
    },

    countrySection: function(id, section, page) {
      window.GLOBALS.location = {
        level: 2,
        code: id
      };
      Backbone.Events.trigger('country:per:clear');
      if(window.GLOBALS.currentSection !== 'countrySection') {
        Backbone.Events.trigger('show:country', id, section, page); // show country
      } else {
        Backbone.Events.trigger('section:show', section, page);
      }
    },

    project: function(id) {
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('project:show', id);
    },

    school: function(id) {
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('school:show', id);
    },

    information: function(id) {
      window.GLOBALS.location = {
        level: 2,
        code: id
      };
      Backbone.Events.trigger('country:per:clear');
      Backbone.Events.trigger('show:country', id); // show country behind the modal
      Backbone.Events.trigger('show:information', id); // show the info inside the modal window
    }

  });

  return Router;
});
