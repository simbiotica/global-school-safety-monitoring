'use strict';

window.GLOBALS = {};

window.GLOBALS.location = {
  level: 0,
  code: 'global'
};

window.GLOBALS.source = {
  current: {
    code: 'official',
    name: 'Official data',
    slug: 'officialOverlay'
  },
  sources: [
    {
      code: 'official',
      name: 'Official data'
    },
    {
      code: 'community',
      name: 'Community data'
    }
  ]
};

window.GLOBALS.currentSection = null;
window.GLOBALS.mapInfoWindow = null;
window.GLOBALS.year = 2013;

require.config({

  paths: {
    jquery: '../../vendor/jquery/jquery',
    jqueryui: '../../lib/jquery-ui/js/jquery-ui-1.10.3.custom',
    mousewheel: '../../vendor/jquery-mousewheel/jquery.mousewheel',
    jscrollpane: '../../vendor/jscrollpane/script/jquery.jscrollpane',
    cartodb: '../../vendor/cartodb.js/dist/cartodb.uncompressed',
    d3: '../../vendor/d3/d3',
    handlebarsLib: '../../vendor/handlebars/handlebars',
    handlebars: 'helpers/handlebars',
    text: '../../vendor/requirejs-text/text',
    sprintf: '../../vendor/sprintf/src/sprintf',
    skinnytip: '../../vendor/skinnytip/skinnytip'
  },

  shim: {

    jquery: {
      exports: '$'
    },

    jqueryui: {
      deps: ['jquery'],
      exports: '$'
    },

    jscrollpane: {
      deps: ['jquery', 'mousewheel'],
      exports: '$'
    },

    cartodb: {
      deps: ['jquery', 'jqueryui'],
      exports: 'cartodb'
    },

    handlebarsLib: {
      exports: 'Handlebars'
    },

    handlebars: {
      deps: ['handlebarsLib'],
      exports: 'Handlebars'
    },

    d3: {
      exports: 'd3'
    },

    sprintf: {
      exports: 'sprintf'
    },

    skinnytip: {
      exports: 'SkinnyTip'
    }
  }

});

require(['cartodb', 'router'], function(cartodb, Routes) {

  // Extensions
  Number.prototype.toCommas = function() {
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  };

  window.GLOBALS.router = new Routes();

});
