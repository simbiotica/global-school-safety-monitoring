define(['handlebarsLib'], function(Handlebars) {
  'use strict';

  // Dots Handlebars
  Handlebars.registerHelper('dots', function(context) {

    function numberToDots(target) {
      var number = target.slice();
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    if (typeof context === 'number') {
      return numberToDots(context.toString());
    }

  });

  // Current section

  Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if(a === b) {
      return opts.fn(this);
    } else {
      return opts.inverse(this);
    }
  });

  return Handlebars;

});
