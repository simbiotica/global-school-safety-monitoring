'use strict';

define(['cartodb', 'sprintf',  '../models/area'], function(cartodb, sprintf, AreaModel) {

  sprintf = sprintf.sprintf;

  var AreasCollection = Backbone.Collection.extend({

    model: AreaModel,

    url: 'http://gpss.cartodb.com/api/v2/sql',

    tables: {
      areas: 'areas_sync_low',
      of_schools: 'of_schools',
      osm_schools: 'osm_schools'
    },

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      return data.rows;
    },

    getAll: function(callback) {
      var fetchOptions, query, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 END) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 END) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 END) AS safe, code, %1$s.name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE %2$s.year <= %3$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid', this.tables.areas, this.tables.of_schools, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, code, %1$s.name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE %2$s.year <= %3$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid', this.tables.areas, this.tables.osm_schools, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getAllReject: function(area_code, callback) {
      var query, fetchOptions, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 END) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 END) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 END) AS safe, code, name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE code!=\'%3$s\' AND %2$s.year <= %4$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid', this.tables.areas, this.tables.of_schools, area_code, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, code, %1$s.name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE code!=\'%3$s\' AND %2$s.year <= %4$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid', this.tables.areas, this.tables.osm_schools, area_code, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByCode: function(area_code, callback) {
      var query, fetchOptions, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 END) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 END) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 END) AS safe, code, name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid', this.tables.areas, this.tables.of_schools, area_code, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, code, %1$s.name, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.area_code=%1$s.code WHERE code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY area_code, %1$s.code, %1$s.name, %1$s.centroid WHERE %2$s.year <= %4$s', this.tables.areas, this.tables.osm_schools, area_code, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    toGeoJSON: function() {
      var features = this.toJSON();
      return {
        type: 'FeatureCollection',
        features: _.map(features, function(f) {
          return {
            type: 'Feature',
            geometry: JSON.parse(f.the_geom),
            properties: f
          };
        })
      };
    }

  });

  return AreasCollection;

});
