'use strict';

define(['cartodb', 'sprintf', '../models/school'], function(cartodb, sprintf, SchoolModel) {

  sprintf = sprintf.sprintf;

  var SchoolsCollection = Backbone.Collection.extend({

    model: SchoolModel,

    url: 'http://gpss.cartodb.com/api/v2/sql',

    tables: {
      of_schools: 'of_schools',
      osm_schools: 'osm_schools'
    },

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      var schools =  _.map(data.rows, function(school) {
        if (!school.school_name || school.school_name === '') {
          school.school_name = 'Unknown';
        }
        school.id = school.cartodb_id;
        school.the_geom = JSON.parse(school.the_geom);
        return school;
      });

      return schools;
    },

    getByCountry: function(country_code, callback) {
      var query, fetchOptions, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT cartodb_id, school_name, country_code, area_code, level, street, location, safe, ST_AsGeoJson(the_geom) AS the_geom FROM %s WHERE safe=true AND country_code=\'%s\'', this.tables.of_schools, country_code);
      } else {
        query = sprintf('SELECT cartodb_id, school_name, country_code, area_code, level, street, location, safe, ST_AsGeoJson(the_geom) AS the_geom FROM %s WHERE safe=true AND country_code=\'%s\'', this.tables.osm_schools, country_code);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          format: 'json',
          q: query
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getById: function(id, callback) {
      var query, fetchOptions, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT cartodb_id, school_name, country_code, area_code, level, street, location, safe, ST_AsGeoJson(the_geom) AS the_geom FROM %s WHERE cartodb_id=\'%s\'', this.tables.of_schools, id);
      } else {
        query = sprintf('SELECT cartodb_id, school_name, country_code, area_code, level, street, location, safe, ST_AsGeoJson(the_geom) AS the_geom FROM %s WHERE cartodb_id=\'%s\'', this.tables.osm_schools, id);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          format: 'json',
          q: query
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    }

  });

  return SchoolsCollection;

});
