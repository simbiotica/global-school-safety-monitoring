'use strict';

define(['cartodb', 'sprintf', '../models/project'], function(cartodb, sprintf, ProjectModel) {

  sprintf = sprintf.sprintf;

  var ProjectsCollection = Backbone.Collection.extend({

    model: ProjectModel,

    url: 'http://gpss.cartodb.com/api/v2/sql',

    table: 'projects',

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      var models = _.map(data.rows, function(m) {
        var current_start_date = new Date(m.start_date);
        var current_end_date = new Date(m.end_date);
        m.id = m.cartodb_id;
        m.start_date = current_start_date.getMonth() + '/' + current_start_date.getFullYear();
        m.end_date = current_end_date.getMonth() + '/' + current_end_date.getFullYear();
        return m;
      });
      return models;
    },

    getAll: function(callback) {
      var query, fetchOptions;

      query = sprintf('SELECT * FROM %s ORDER BY cartodb_id DESC', this.table);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getById: function(project_id, callback) {
      var query, fetchOptions;

      query = sprintf('SELECT * FROM %s WHERE cartodb_id=\'%s\'', this.table, project_id);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByArea: function(area_code, callback) {
      var query, fetchOptions;

      query = sprintf('SELECT * FROM %s WHERE area_code=\'%s\' ORDER BY cartodb_id DESC', this.table, area_code);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByCountry: function(country_code, callback) {
      var query, fetchOptions;

      query = sprintf('SELECT * FROM %s WHERE country_code=\'%s\' ORDER BY cartodb_id DESC', this.table, country_code);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    }

  });

  return ProjectsCollection;

});
