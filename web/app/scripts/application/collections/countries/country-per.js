'use strict';

define(['cartodb', 'sprintf', '../../models/country'], function(cartodb, sprintf, CountryModel) {

  sprintf = sprintf.sprintf;

  var CountryPeruCollection = Backbone.Collection.extend({

    model: CountryModel,

    url: 'http://gpss.cartodb.com/api/v2/sql',

    tables: {
      schools: {
        national: 'all_georeferenced_schools_with_reform_final_2'
      }
    },

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      return data.rows;
    },

    getByNational: function(callback) {
      var fetchOptions, query;

      query = sprintf('SELECT count(CASE WHEN cartodb_id IS NOT NULL THEN 1 ELSE 0 END) AS total ,sum(CASE WHEN area = \'RURAL\' THEN 1 ELSE 0 END) AS rural ,sum(CASE WHEN area = \'URBAN\' THEN 1 ELSE 0 END) AS urban FROM %s', this.tables.schools.national);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByLima: function(callback) {
      var fetchOptions, query;

      query = sprintf('SELECT count(cartodb_id) as total FROM %s WHERE departamen = \'LIMA METROPOLITANA\'', this.tables.schools.national);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByRegions: function(callback) {
      var fetchOptions, query;

      query = sprintf('SELECT departamen, replace(lower(departamen),\' \',\'-\') as dep_id, lower(departamen) as name FROM %s GROUP BY departamen ORDER BY departamen ASC', this.tables.schools.national);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByRegionalData: function(callback) {
      var fetchOptions, query;

      query = sprintf('SELECT count(cartodb_id) as value, departamen, concepto2 FROM %s GROUP BY departamen, concepto2 ORDER BY concepto2 ASC', this.tables.schools.national);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    riskList: function() {
      return [
        { name: 'Maintenance', value: '#BF3944' },
        { name: 'Retrofitting', value: '#14E0C0' },
        { name: 'Rehabilitation', value: '#FFEC00' },
        { name: 'Rehabilitation & Retrofitting', value: '#5491F6' },
        { name: 'Replacement', value: '#7E00B2' },
        { name: 'Combined intervention (same school undergoes maintenance, rehabilitation and retrofitting)', value: '#85B200' },
        { name: 'Insufficient information', value: '#FFA1F0' }
      ];
    },

    toGeoJSON: function() {
      var features = this.toJSON();
      return {
        type: 'FeatureCollection',
        features: _.map(features, function(f) {
          return {
            type: 'Feature',
            geometry: JSON.parse(f.the_geom),
            properties: f
          };
        })
      };
    }

  });

  return CountryPeruCollection;

});
