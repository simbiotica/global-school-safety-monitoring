define(['cartodb', '../models/hazard'], function(cartodb, HazardModel) {
  'use strict';

  var HazardsCollection = Backbone.Collection.extend({

    model: HazardModel,

    url: 'https://gpss.cartodb.com/api/v2/sql?q=SELECT slug, name, url, layer FROM hazard_layers WHERE active=true',

    parse: function(data) {
      return data.rows;
    }

  });

  return HazardsCollection;

});
