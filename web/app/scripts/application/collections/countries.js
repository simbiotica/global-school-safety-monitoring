'use strict';

define(['cartodb', 'sprintf', '../models/country'], function(cartodb, sprintf, CountryModel) {

  sprintf = sprintf.sprintf;

  var CountriesCollection = Backbone.Collection.extend({

    model: CountryModel,

    url: 'http://gpss.cartodb.com/api/v2/sql',

    tables: {
      areas: 'areas_sync_low',
      countries: 'country_sync_low',
      of_schools: 'of_schools',
      osm_schools: 'osm_schools'
    },

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      return data.rows;
    },

    getByArea: function(area_code, callback) {
      var fetchOptions, query, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 end) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 end) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 end) AS safe, %1$s.name, country_code AS code, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.area_code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY %1$s.name, country_code, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.of_schools, area_code, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, %1$s.name, country_code AS code, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.area_code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY %1$s.name, country_code, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.osm_schools, area_code, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByAreaReject: function(country_code, area_code, callback) {
      var fetchOptions, query, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 end) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 end) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 end) AS safe, country_code AS code, %1$s.name, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.country_code!=\'%3$s\' AND %2$s.area_code=\'%4$s\' AND %2$s.year <= %5$s GROUP BY %1$s.name, country_code, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.of_schools, country_code, area_code, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, country_code AS code, %1$s.name, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.country_code!=\'%3$s\' AND %2$s.area_code=\'%4$s\' AND %2$s.year <= %5$s GROUP BY %1$s.name, country_code, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.osm_schools, country_code, area_code, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          format: 'json',
          q: query
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByCode: function(country_code, callback) {
      var fetchOptions, query, source;

      source = window.GLOBALS.source.current.code;

      if (source === 'official') {
        query = sprintf('SELECT sum(CASE WHEN safe_code = 0 THEN 1 ELSE 0 end) AS unknown, sum(CASE WHEN safe_code = 1 THEN 1 ELSE 0 end) AS unsafe, sum(CASE WHEN safe_code = 2 THEN 1 ELSE 0 end) AS safe, country_code AS code, %1$s.area_code, %1$s.name, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.country_code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY country_code, %1$s.area_code, %1$s.name, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.of_schools, country_code, window.GLOBALS.year);
      } else if (source === 'community') {
        query = sprintf('SELECT count(1) AS unknown, 0 AS unsafe, 0 AS safe, country_code AS code, %1$s.area_code, %1$s.name, %1$s.description, %1$s.centroid AS the_geom FROM %2$s JOIN %1$s ON %2$s.country_code=%1$s.code WHERE %2$s.country_code=\'%3$s\' AND %2$s.year <= %4$s GROUP BY country_code, %1$s.area_code, %1$s.name, %1$s.description, %1$s.centroid', this.tables.countries, this.tables.osm_schools, country_code, window.GLOBALS.year);
      }

      fetchOptions = _.extend(this.options, {
        data: {
          format: 'json',
          q: query
        },
        success: function(collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    toGeoJSON: function() {
      var features = this.toJSON();
      return {
        type: 'FeatureCollection',
        features: _.map(features, function(f) {
          return {
            type: 'Feature',
            geometry: JSON.parse(f.the_geom),
            properties: f
          };
        })
      };
    }

  });

  return CountriesCollection;

});
