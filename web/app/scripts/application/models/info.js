'use strict';

define(['cartodb', 'sprintf'], function(cartodb, sprintf) {

  sprintf = sprintf.sprintf;

  var InfoModel = Backbone.Model.extend({

    url: 'http://gpss.cartodb.com/api/v2/sql',

    table: 'of_schools',

    options: {
      dataType: 'json',
      error: function(xhr, err) {
        throw err.textStatus;
      }
    },

    parse: function(data) {
      var model = {};
      _.each(data.rows[0], function(v, k) {
        model[k] = (v) ? v : 0;
      });
      return model;
    },

    getGlobal: function(callback) {
      var query, fetchOptions;

      query = sprintf('select sum(case when safe_code = 0 then 1 else 0 end) as unknown, sum(case when safe_code = 1 then 1 else 0 end) as unsafe, sum(case when safe_code = 2 then 1 else 0 end) as safe FROM %s WHERE year <= %s', this.table, window.GLOBALS.year);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(data, collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByArea: function(area_code, callback) {
      var fetchOptions, query;

      query = sprintf('select sum(case when safe_code = 0 then 1 else 0 end) as unknown, sum(case when safe_code = 1 then 1 else 0 end) as unsafe, sum(case when safe_code = 2 then 1 else 0 end) as safe FROM %s WHERE area_code=\'%s\' AND year <= %s', this.table, area_code, window.GLOBALS.year);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(data, collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    },

    getByCountry: function(country_code, callback) {
      var fetchOptions, query;

      query = sprintf('select sum(case when safe_code = 0 then 1 else 0 end) as unknown, sum(case when safe_code = 1 then 1 else 0 end) as unsafe, sum(case when safe_code = 2 then 1 else 0 end) as safe FROM %s WHERE country_code=\'%s\' AND year <= %s', this.table, country_code, window.GLOBALS.year);

      fetchOptions = _.extend(this.options, {
        data: {
          q: query,
          format: 'json'
        },
        success: function(data, collection) {
          if (callback && typeof callback === 'function') {
            callback(collection);
          }
        }
      });

      this.fetch(fetchOptions);
    }

  });

  return InfoModel;

});
