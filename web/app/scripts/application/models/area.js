define(['cartodb'], function() {
  'use strict';

  var AreaModel = Backbone.Model.extend({});

  return AreaModel;

});
