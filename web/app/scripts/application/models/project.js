'use strict';

define(['cartodb'], function() {

  var ProjectModel = Backbone.Model.extend({

    defaults: {
      name: null, // String
      description: null, // String
      body: null, // String
      country_code: null, // String
      area_code: null, // String
      start_date: null, // Date
      end_date: null, // Date
      amount: null // String
    }

  });

  return ProjectModel;

});
