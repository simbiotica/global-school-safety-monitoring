'use strict';

require.config({

  paths: {
    jquery: '../../vendor/jquery/jquery',
    chachislider: '../../lib/chachi-slider/jquery.chachi-slider',
    underscore: '../../vendor/underscore/underscore',
    backbone: '../../vendor/backbone/backbone',
    spinner: '../../vendor/spinjs/spin',
    ready: '../../vendor/requirejs-domready/domReady',
    eventie: '../../vendor/eventie',
    eventEmitter: '../../vendor/eventEmitter',
    imagesloaded: '../../vendor/imagesloaded/imagesloaded',
  },

  shim: {

    jquery: {
      exports: '$'
    },

    chachislider: {
      deps: ['jquery'],
      exports: '$'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    }

  }

});

require(['chachislider', 'backbone', 'spinner', 'imagesloaded', 'ready'], function($, Backbone, Spinner, imagesLoaded, domReady) {

  domReady(function() {

    var SliderView = Backbone.View.extend({

      el: '#slider',

      events: {
        'click #knowmore': 'nextSlide'
      },

      initialize: function() {
        this.$el.chachiSlider();
        this.$next = $('.chachi-slide-next');

        Backbone.Events.on('images:loaded', this.show, this);
      },

      nextSlide: function(e) {
        if (e.preventDefault) {
          e.preventDefault();
        }
        this.$next.trigger('click');
      },

      show: function() {
        this.$el.fadeIn('fast');
      }

    });

    var SpinnerView = Backbone.View.extend({

      el: '#spinner',

      options: {
        lines: 11, // The number of lines to draw
        length: 10, // The length of each line
        width: 5, // The line thickness
        radius: 10, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#fff', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 'auto', // Top position relative to parent in px
        left: 'auto' // Left position relative to parent in px
      },

      initialize: function() {
        this.spinner = new Spinner(this.options);
        this.start();

        Backbone.Events.on('images:loaded', this.stop, this);
      },

      start: function() {
        this.spinner.spin(this.el);
      },

      stop: function() {
        this.spinner.stop();
      }

    });

    new SpinnerView();
    new SliderView();

    imagesLoaded('img', function() {

      Backbone.Events.trigger('images:loaded');

    });

  });

});
