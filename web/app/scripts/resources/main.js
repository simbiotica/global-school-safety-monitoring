'use strict';

require.config({

  paths: {
    backbone: '../../vendor/backbone/backbone',
    underscore: '../../vendor/underscore/underscore-min',
    jquery: '../../vendor/jquery/jquery',
    jqueryui: '../../lib/jquery-ui/js/jquery-ui-1.10.3.custom',
    handlebars: '../../vendor/handlebars/handlebars',
    text: '../../vendor/requirejs-text/text'
  },

  shim: {

    jquery: {
      exports: '$'
    },

    jqueryui: {
      deps: ['jquery'],
      exports: '$'
    },

    handlebarsLib: {
      exports: 'Handlebars'
    },

    handlebars: {
      exports: 'Handlebars'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    },
  }

});

require(['backbone', 'router'], function(Backbone, Routes) {

  new Routes();

});
