'use strict';

define([
	'backbone',
	'handlebars',
  'jqueryui',
	'text!../../../templates/resources/resources.handlebars',
  // 'text!data/resources.json'
], function(Backbone, Handlebars, $, TPL) {

  var ResourcesView = Backbone.View.extend({

    el: '#resources-content',

    events: {
      'click .nav-content a': 'goToContent'
    },

    template: Handlebars.compile(TPL),

    options: {
      active: 0,
      collapsive: false,
      heightStyle: 'content'
    },

    initialize: function() {
      this.$body = $('body, html');
      this.$content = $('#resources-content');
      this.y = this.$content.offset().top;

      var currentUrl = window.location.href;
      if(currentUrl.search(/#/) !== -1) {
        var urlParam = currentUrl.split('#');
        var elem = '#'+urlParam[1];
        this.goToContent(null, elem, true);
      }
    },

    render: function(resourcesJSON) {
      var data = JSON.parse(resourcesJSON);
      var sortedData = [];
      
      _.each(data, function(group){
        _.each(group, function(nextGroup,i){
          var sorted = _.sortBy(nextGroup, function(item){ return parseInt(item.year,10); });
          nextGroup = sorted.reverse();
          sortedData[i]= nextGroup;
        });
      });

      this.$el.html(this.template({data: sortedData}));
    },

    _scrollTo: function(noDelay) {
      var delay = 50;
      var speed = 500;

      if(noDelay) {
        delay = 0;
        speed = 0;
      }

      this.$body.stop().delay(delay).animate({
        scrollTop: this.y - 110
      }, speed, 'swing');
    },

    goToContent: function(ev, elem, noDelay) {
      if(ev) {
        ev.preventDefault();
      }

      var hash, page, $currentEl, $content;

      if(!elem) {
        $currentEl = $(ev.currentTarget);
        hash = $currentEl.attr('href');
      } else {
        hash = elem;
      }

      page = hash.replace(/#/, '');
      $content = this.$(hash);

      window.history.replaceState({}, page, hash);

      this.y = $content.offset().top;
      this._scrollTo(noDelay);
    }

  });

  return ResourcesView;

});
