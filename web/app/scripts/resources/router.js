'use strict';

define([
  'backbone',
  'views/resources'
], function(Backbone, ResourcesView) {

  var Router;

  Router = Backbone.Router.extend({

    routes: {
      '': 'index',
      '*notFound': 'index'
    },

    initialize: function() {
      Backbone.history.start();
    },

    index: function() {
      new ResourcesView();
    }

  });

  return Router;
});
