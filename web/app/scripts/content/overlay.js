'use strict';

define(['backbone', 'handlebars', 'text!../../templates/overlay.handlebars'], function(Backbone, Handlebars, tpl) {

  var OverlayView = Backbone.View.extend({

    el: '#overlay',

    events: {
      'click #overlayCloseBtn': 'hide',
      'mouseout .overlay-content': 'onMouseOut',
      'mouseover .overlay-content': 'onMouseOver'
    },

    template: Handlebars.compile(tpl),

    initialize: function() {
      this.$body = $('body');
      this.data = {};
      Backbone.Events.on('overlay:show', this.show, this);
    },

    render: function() {
      this.$el.html(this.template({content: this.data})).fadeIn('fast');
    },

    show: function(data) {
      if (data) {
        this.data = data;
      } else {
        this.data = '';
      }
      this.$body.css('overflow', 'hidden');
      this.render();
    },

    hide: function() {
      this.$body.css('overflow', 'auto');
      this.$el.fadeOut('fast');
    },

    onMouseOut: function() {
      var self = this;

      this.$el.on('click', function() {
        self.hide();
      });
    },

    onMouseOver: function() {
      var self = this;

      this.$el.off('click');
      $('#overlayCloseBtn').on('click', function(e) {
        e.preventDefault();
        self.hide();
      });
    }

  });

  return OverlayView;

});
