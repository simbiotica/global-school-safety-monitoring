'use strict';

define(['jqueryui', 'backbone'], function($, Backbone) {

  var AboutView = Backbone.View.extend({

    el: '#about',

    events: {
      'click a[data-overlay]': 'openOverlay',
      'click .nav-content a': 'goToContent'
    },

    options: {
      active: 0,
      collapsive: false,
      heightStyle: 'content'
    },

    initialize: function() {
      this.$body = $('body, html');
      this.$bg = this.$el.find('.bg-content');
      this.$content = $('#about-content');
      this.y = this.$content.offset().top;

      var currentUrl = window.location.href;
      if(currentUrl.search(/#/) !== -1) {
        var urlParam = currentUrl.split('#');
        var elem = '#'+urlParam[1];
        this.goToContent(null, elem, true);
      }
    },

    _scrollTo: function(noDelay) {
      var delay = 50;
      var speed = 500;

      if(noDelay) {
        delay = 0;
        speed = 0;
      }

      this.$body.stop().delay(delay).animate({
        scrollTop: this.y - 110
      }, speed, 'swing');
    },

    goToContent: function(ev, elem, noDelay) {
      if(ev) {
        ev.preventDefault();
      }

      var hash, page, $currentEl, $content;

      if(!elem) {
        $currentEl = $(ev.currentTarget);
        hash = $currentEl.attr('href');
      } else {
        hash = elem;
      }

      page = hash.replace(/#/, '');
      $content = this.$(hash);

      window.history.replaceState({}, page, hash);

      this.y = $content.offset().top;
      this._scrollTo(noDelay);
    },

    openOverlay: function(e) {
      e.preventDefault();
      Backbone.Events.trigger('overlay:show', $('#' + $(e.currentTarget).data('overlay')).html());
    }

  });

  return AboutView;

});
