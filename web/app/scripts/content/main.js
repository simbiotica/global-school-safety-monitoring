'use strict';

require.config({

  paths: {
    jquery: '../../vendor/jquery/jquery',
    jqueryui: '../../lib/jquery-ui/js/jquery-ui-1.10.3.custom',
    underscore: '../../vendor/underscore/underscore',
    backbone: '../../vendor/backbone/backbone',
    select2: '../../vendor/select2/select2',
    handlebars: '../../vendor/handlebars/handlebars',
    text: '../../vendor/requirejs-text/text',
    ready: '../../vendor/requirejs-domready/domReady'
  },

  shim: {

    jquery: {
      exports: '$'
    },

    jqueryui: {
      deps: ['jquery'],
      exports: '$'
    },

    select2: {
      deps: ['jquery'],
      exports: '$'
    },

    handlebars: {
      exports: 'Handlebars'
    },

    underscore: {
      exports: '_'
    },

    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    }
  }

});

require(['select2', 'about', 'overlay', 'ready'], function($, AboutView, OverlayView, domReady) {

  domReady(function() {
    if (document.getElementById('about')) {
      new AboutView();
      new OverlayView();
    }
  });

  $('select').select2({
    width: '162px'
  });

});
