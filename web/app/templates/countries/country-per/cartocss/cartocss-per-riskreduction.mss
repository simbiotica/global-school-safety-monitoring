#all_georeferenced_schools_with_reform_final_2 {
  marker-line-width: 0;
  marker-allow-overlap: false;
  marker-opacity: 1;
}
#all_georeferenced_schools_with_reform_final_2 {
  marker-fill:#FFA1F0;
}

#all_georeferenced_schools_with_reform_final_2 [concepto2="Maintenance"] {
  marker-fill:#BF3944;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Retrofitting"] {
  marker-fill:#14E0C0;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Rehabilitation"] {
  marker-fill:#FFEC00;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Rehabilitation & Retrofitting"] {
  marker-fill:#5491F6;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Replacement"] {
  marker-fill:#7E00B2;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Combined intervention (same school undergoes maintenance, rehabilitation and retrofitting)"] {
  marker-fill:#85B200;
}
#all_georeferenced_schools_with_reform_final_2 [concepto2="Insufficient information"] {
  marker-fill:#FFA1F0;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<6] {
  marker-width: 5;
}

#all_georeferenced_schools_with_reform_final_2 [zoom>=6] {
  marker-width: 4;
}
#all_georeferenced_schools_with_reform_final_2 [zoom>=8] {
  marker-width: 7;
}

#all_georeferenced_schools_with_reform_final_2 [zoom>=12] {
  marker-width: 9;
}