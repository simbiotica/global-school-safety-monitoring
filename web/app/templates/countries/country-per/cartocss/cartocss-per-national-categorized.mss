#all_georeferenced_schools_with_reform_final_2 {
  marker-allow-overlap: false;
  marker-width: 4;
  marker-fill-opacity: 0.75;
  marker-line-width: 1;
}
#all_georeferenced_schools_with_reform_final_2 [area='RURAL'] {
  marker-fill:#FFC500;
  marker-line-color: #FFC500;
}
#all_georeferenced_schools_with_reform_final_2 [area='URBAN'] {
  marker-fill:#DF4AA7;
  marker-line-color: #DF4AA7;
}
