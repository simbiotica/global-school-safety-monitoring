#all_georeferenced_schools_with_reform_final_2 {
  marker-allow-overlap: false;
  marker-fill: #3f98ca;
  marker-fill-opacity: 1;
  marker-line-width: 0;
}
#all_georeferenced_schools_with_reform_final_2 [zoom>12] {
  marker-width: 6;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<4] {
  marker-width: 6;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<7] {
  marker-width: 6;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<=11] {
  marker-width: 6;
}
#all_georeferenced_schools_with_reform_final_2 [zoom>11] {
  marker-width: 6;
  marker-allow-overlap: false;
}