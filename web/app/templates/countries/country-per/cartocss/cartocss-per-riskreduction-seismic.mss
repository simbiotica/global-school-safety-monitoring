#exp_final {
  marker-fill-opacity: 0.9;
  marker-allow-overlap: false;
  marker-line-width: 0;
  marker-width: 6;
}

#exp_final[escen="Good structural behavior"] {
   marker-fill: #56E602;
}
#exp_final[escen="High damage potential"] {
   marker-fill: #1F78B4;
}
#exp_final[escen="High collapse potential"] {
   marker-fill: #E62000;
}
#exp_final[escen="Retrofitted"] {
   marker-fill: #33A02C;
}