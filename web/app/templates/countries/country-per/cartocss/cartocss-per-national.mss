#all_georeferenced_schools_with_reform_final_2 {
  marker-allow-overlap: false;
  marker-fill:#9DA19D;
  marker-fill-opacity: 1;
  marker-line-width: 0;
}
#all_georeferenced_schools_with_reform_final_2 [zoom>12] {
  marker-width: 6;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<4] {
  marker-width: 3;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<7] {
  marker-width: 2;
}
#all_georeferenced_schools_with_reform_final_2 [zoom<=11] {
  marker-width: 3;
}
#all_georeferenced_schools_with_reform_final_2 [zoom>11] {
  marker-width: 6;
  marker-allow-overlap: false;
}