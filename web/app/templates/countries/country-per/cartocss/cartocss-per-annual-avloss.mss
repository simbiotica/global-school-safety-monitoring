#images_g_and_j{
  marker-fill-opacity: 1;
  marker-line-width: 0;
  marker-line-opacity: 1;
  marker-width: 6;
  marker-fill: #FFFFB2;
  marker-allow-overlap: false;
}
#images_g_and_j [ pae_valfis___annual_loss_exposure_value <= 0.154471434407] {
   marker-fill: #3CA900;
}
#images_g_and_j [ pae_valfis___annual_loss_exposure_value <= 0.0275954583179] {
   marker-fill: #84C701;
}
#images_g_and_j [ pae_valfis___annual_loss_exposure_value <= 0.0205412575129] {
   marker-fill: #FCFC00;
}
#images_g_and_j [ pae_valfis___annual_loss_exposure_value <= 0.0162325918997] {
   marker-fill: #F17F01;
}
#images_g_and_j [ pae_valfis___annual_loss_exposure_value <= 0.0130514570614] {
   marker-fill: #FFFFB2;
}