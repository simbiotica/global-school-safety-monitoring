define([
    'chai',
    'app/scripts/models/wmstile'
], function(Chai, WMSTileModel) {
    'use strict';

    var expect = Chai.expect;
    var WMSTile = new WMSTileModel();

    describe('#Model: WMSTile', function() {

        describe('@Create', function() {
            it('WMSTile should a instance of WMSTileModel', function() {
                expect(WMSTile).to.instanceOf(WMSTileModel);
            });
        });

    });

});
