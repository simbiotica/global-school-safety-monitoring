define([
    'chai',
    'app/scripts/models/school'
], function(Chai, SchoolModel) {
    'use strict';

    var expect = Chai.expect;
    var school = new SchoolModel();

    describe('#Model: School', function() {

        describe('@Create', function() {
            it('school should a instance of SchoolModel', function() {
                expect(school).to.instanceOf(SchoolModel);
            });
        });

    });

});
