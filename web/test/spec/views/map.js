define([
    'chai',
    'app/scripts/views/map'
], function(Chai, MapView) {
    'use strict';

    var expect = Chai.expect;
    var map = new MapView();

    describe('#View: Map', function() {

        this.timeout(5000);

        describe('@Create', function() {

            it('map should a instance MapView', function() {
                expect(map).to.instanceOf(MapView);
            });

        });

        describe('@Layers', function() {

            it('tiles should load correctly', function() {
                expect(map.tiles).to.be.an('object');
            });

            it('WMSTiles should load correctly', function(done) {
                map.setWMSTiles('raster11', function(tile) {
                    expect(tile).to.be.an('object');
                    done();
                });
            });

            it('cartodb layer should create correctly', function(done) {
                map.setCartoDBLayer(function(layer) {
                    expect(layer.getSubLayer(0)).to.exist;
                    done();
                });
            });

            it('world layer should show correctly', function(done) {
                map.showWorld(function(layer) {
                    expect(layer.getSubLayer(0)).to.exist;
                    expect(layer.getSubLayer(1)).to.exist;
                    expect(layer.getSubLayer(0)).to.be.an('object');
                    expect(layer.getSubLayer(1)).to.be.an('object');
                    done();
                });
            });

        });

    });

});
