define([
    'chai',
    'app/scripts/views/list'
], function(Chai, ListView) {
    'use strict';

    var expect = Chai.expect;
    var list = new ListView();

    describe('#Views: List', function() {

        describe('@Create', function() {

            it('list should a instance ListView', function() {
                expect(list).to.instanceOf(ListView);
            });

            it('list should initialize jquery tabs', function() {
                expect(list.$el).to.have.property('tabs');
            });

        });

        describe('@Show', function() {

            it('get world data should be correctly', function(done) {
                list.showWorld(function(collection) {
                    expect(collection.length).to.be.above(0);
                    done();
                });
            });

        });

    });

});
