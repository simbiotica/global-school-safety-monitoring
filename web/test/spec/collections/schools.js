define([
    'chai',
    'app/scripts/collections/schools'
], function(Chai, SchoolsCollection) {
    'use strict';

    var expect, schools;

    SchoolsCollection = SchoolsCollection.extend({
        queries: {
            getAll: 'SELECT * FROM osm_schools LIMIT 1'
        }
    });
    expect = Chai.expect;
    schools = new SchoolsCollection();

    describe('#Collection: Schools', function() {

        this.timeout(5000);

        describe('@Create', function() {

            it('schools should a instance of SchoolsCollection', function() {
                expect(schools).to.instanceOf(SchoolsCollection);
            });

        });

        describe('@Fetch', function() {

            it('fetchCountries method should exist', function() {
                expect(schools.fetchSchools).to.exist;
            });

            it('fetch data should be correctly', function(done) {
                schools.fetchSchools({
                    success: function(data) {
                        expect(data).to.be.an('object');
                        done();
                    },
                    error: function(errors) {
                        expect(errors).to.not.exist;
                        done();
                    }
                });
            });

            it('save data should be correctly', function() {
                expect(schools.length).to.be.equal(1);
            });

        });

        describe('@Get', function() {

            it('get methods should return a object', function() {
                expect(schools.at(0)).to.be.an('object');
                expect(schools.where({amenity: 'school'})).to.be.an('array');
            });

        });

    });

});
