define([
    'chai',
    'app/scripts/collections/wmstiles'
], function(Chai, WMSTilesCollection) {
    'use strict';

    var expect, WMSTiles;

    expect = Chai.expect;
    WMSTiles = new WMSTilesCollection();

    describe('#Collection: WMSTiles', function() {

        this.timeout(5000);

        describe('@Create', function() {

            it('WMSTiles should a instance of WMSTilesCollection', function() {
                expect(WMSTiles).to.instanceOf(WMSTilesCollection);
            });

        });

        describe('@Fetch', function() {

            it('fetch data should be correctly', function(done) {
                WMSTiles.fetch({
                    success: function(collection) {
                        expect(collection.length).to.be.above(0);
                        done();
                    },
                    error: function(errors) {
                        expect(errors).to.not.exist;
                        done();
                    }
                });
            });

        });

        describe('@Get', function() {

            it('get layer should return a object', function() {
                expect(WMSTiles.where({layer: 'raster11'})[0]).to.be.an('object');
            });

        });

    });

});
