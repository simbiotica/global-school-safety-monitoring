require.config({
    baseUrl: '../',
    paths: {
        jquery: 'app/vendor/jquery/jquery',
        jqueryui: 'app/lib/jquery-ui/js/jquery-ui-1.10.3.custom',
        mousewheel: 'app/vendor/jquery-mousewheel/jquery.mousewheel',
        cartodb: 'app/lib/cartodb/js/cartodb.nojquery',
        d3: 'app/vendor/d3/d3',
        handlebars: 'app/vendor/handlebars/handlebars',
        text: 'app/vendor/requirejs-text/text',
        mocha: 'test/vendor/mocha/mocha',
        chai: 'test/vendor/chai/chai',
    },
    shim: {
        jquery: {
            exports: '$'
        },
        jqueryui: {
            deps: ['jquery'],
            exports: '$'
        },
        cartodb: {
            deps: ['jquery', 'jqueryui'],
            exports: 'cartodb'
        },
        d3: {
            exports: 'd3'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        mocha: {
            exports: 'mocha'
        }
    }
});

require(['require', 'mocha'], function(require, mocha) {
    'use strict';

    mocha.setup('bdd');

    require([
        'test/spec/models/school',
        'test/spec/models/wmstile',
        'test/spec/collections/schools',
        'test/spec/collections/wmstiles',
        'test/spec/views/map',
        'test/spec/views/list'
    ], function() {
        (window.mochaPhantomJS || mocha).run();
    });
});
