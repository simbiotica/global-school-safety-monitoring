## Requeriments

* PHP 5.4+
* Node 0.8+

Bower and Grunt client installed globaly:

	npm install -g grunt-cli bower


## To install

    curl -sS https://getcomposer.org/installer | php
    php composer.phar install
    npm install && bower install
	cap deploy:setup

## To run

**First duplicate and rename `web/.htaccess.dist` to `web/.htaccess`.**

On development (web/index_dev.php)
    
    grunt

On production (web/index.php)
    
    grunt build

Testing

    grunt test


Deploy first time

    cap deploy:setup
    cap deploy

Deploy

    cap deploy