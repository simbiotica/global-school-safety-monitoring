<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\HttpCacheServiceProvider;

$app = new Application();

$app->register(new HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => __DIR__.'/cache/',
));
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
    'twig.options' => array('cache' => __DIR__.'/../cache/twig')
));
$app->register(new UrlGeneratorServiceProvider());

function ie($version) {
    if(strpos($_SERVER['HTTP_USER_AGENT'], $version) !== false) { 
        return true;
    }
    return false;
}  

/* Routes */
$app->get('/', function() use ($app) {
    $body = $app['twig']->render('homepage.twig', array(
        'title' => 'Welcome',
    ));

    return new Response($body, 200, array(
        'Cache-Control' => 's-maxage=86400, public',
    ));
})->bind('homepage');

if (ie('MSIE 9.') == true || ie('MSIE 8.') == true || ie('MSIE 7.') == true || ie('MSIE 6.') == true) { 

    $app->get('/map', function() use ($app) {
        $body = $app['twig']->render('browser.twig', array(
            'title' => 'Browser not supported',
        ));

        return new Response($body, 200, array(
            'Cache-Control' => 's-maxage=86400, public',
        ));
    })->bind('map');

} else {
    $app->get('/map', function() use ($app) {
        $body = $app['twig']->render('application.twig', array(
            'title' => 'Map',
        ));

        return new Response($body, 200, array(
            'Cache-Control' => 's-maxage=86400, public',
        ));
    })->bind('map');
}

$app->get('/about', function() use ($app) {
    $body = $app['twig']->render('about.twig', array(
        'title' => 'About',
    ));

    return new Response($body, 200, array(
        'Cache-Control' => 's-maxage=86400, public',
    ));
})->bind('about');

$app->get('/resources', function() use ($app) {
    $body = $app['twig']->render('resources.twig', array(
        'title' => 'Resources',
    ));

    return new Response($body, 200, array(
        'Cache-Control' => 's-maxage=86400, public',
    ));
})->bind('resources');

$app->get('/contact', function() use ($app) {
    $body = $app['twig']->render('contact.twig', array(
        'title' => 'Contact',
    ));

    return new Response($body, 200, array(
        'Cache-Control' => 's-maxage=86400, public',
    ));
})->bind('contact');

?>
