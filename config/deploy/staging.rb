set :stage, :staging

role :app, %w{root@176.58.109.211}
role :web, %w{root@176.58.109.211}
role :db,  %w{root@176.58.109.211}

server '176.58.109.211', user: 'root', roles: %w{web app}, password: fetch(:password)