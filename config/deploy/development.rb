set :stage, :development
set :branch, 'dev'

role :app, %w{root@eva}
role :web, %w{root@eva}
role :db,  %w{root@eva}

server 'eva', user: 'root', roles: %w{web app}, password: fetch(:password)
