'use strict';

module.exports = function(grunt) {

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    root: 'web/',

    clean: {
      app: ['<%= root %>.tmp'],
      dist: ['<%= root %>.tmp', '<%= root %>dist']
    },

    jshint: {
      files: [
        'Gruntfile.js',
        '<%= root %>app/scripts/{,*/}*{,*/}*.js',
        // '<%= root %>test/spec/{,*/}*{,*/}*.js',
        // '<%= root %>test/specRunner.js'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    mocha_phantomjs: {
      all: ['<%= root %>test/index.html']
    },

    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= root %>app',
          dest: '<%= root %>dist',
          src: [
            'fonts/{,*/}*{,*/}*',
            'lib/*{,*/}*{,*/}*',
            'templates/{,*/}*{,*/}*'
          ]
        }]
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= root %>app/images',
          src: '{,*/}*{,*/}*.{png,jpg,jpeg}',
          dest: '<%= root %>dist/images'
        }]
      },
      static: {
        files: {
          '<%= root %>dist/styles/select2.png': '<%= root %>app/vendor/select2/select2.png',
          '<%= root %>dist/styles/select2x2.png': '<%= root %>app/vendor/select2/select2x2.png',
          '<%= root %>dist/styles/select2-spinner.gif': '<%= root %>app/vendor/select2/select2-spinner.gif'
        }
      }
    },

    compass: {
      options: {
        sassDir: '<%= root %>app/styles',
        cssDir: '<%= root %>.tmp/styles',
        imagesDir: '<%= root %>app/images',
        generatedImagesDir: '<%= root %>.tmp/images/sprites',
        javascriptsDir: '<%= root %>app/scripts',
        fontsDir: '<%= root %>app/fonts',
        importPath: '<%= root %>app/vendor',
      },
      dist: {
        options: {
          generatedImagesDir: '<%= root %>dist/images/sprites',
          httpImagesPath: '/dist/images',
          httpGeneratedImagesPath: '/dist/images/sprites',
          httpJavascriptsPath: '/dist/scripts',
          httpFontsPath: '/dist/fonts',
          environment: 'production',
          relativeAssets: false
        }
      },
      app: {
        options: {
          debugInfo: true,
          assetCacheBuster: false,
          environment: 'development',
          relativeAssets: true
        }
      }
    },

    cssmin: {
      dist: {
        files: {
          // Application
          '<%= root %>dist/styles/application.css': [
            '<%= root %>app/vendor/normalize-css/normalize.css',
            '<%= root %>app/vendor/cartodb.js/dist/cartodb.css',
            '<%= root %>app/vendor/jscrollpane/style/jquery.jscrollpane.css',
            '<%= root %>app/lib/jquery-ui/css/jquery-ui-1.10.3.custom.css',
            '<%= root %>.tmp/styles/application.css'
          ],
          // Homepage
          '<%= root %>dist/styles/homepage.css': [
            '<%= root %>app/vendor/normalize-css/normalize.css',
            '<%= root %>app/lib/chachi-slider/chachi-slider.css',
            '<%= root %>.tmp/styles/homepage.css'
          ],
          // Content
          '<%= root %>dist/styles/content.css': [
            '<%= root %>app/vendor/normalize-css/normalize.css',
            '<%= root %>app/vendor/select2/select2.css',
            '<%= root %>app/lib/jquery-ui/css/jquery-ui-1.10.3.custom.css',
            '<%= root %>.tmp/styles/content.css'
          ]
        }
      }
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'],
        map: {
          prev: '<%= root %>.tmp/styles/'
        }
      },
      compile: {
        files: [{
          expand: true,
          cwd: '<%= root %>.tmp/styles/',
          src: '{,*/}*.css',
          dest: '<%= root %>.tmp/styles/'
        }]
      }
    },

    requirejs: {
      options: {
        include: 'main',
        optimize: 'uglify',
        preserveLicenseComments: false,
        useStrict: true,
        wrap: false
      },
      application: {
        options: {
          baseUrl: '<%= root %>app/scripts/application',
          out: '<%= root %>dist/scripts/application.js',
          mainConfigFile: '<%= root %>app/scripts/application/main.js',
        }
      },
      homepage: {
        options: {
          baseUrl: '<%= root %>app/scripts/homepage',
          out: '<%= root %>dist/scripts/homepage.js',
          mainConfigFile: '<%= root %>app/scripts/homepage/main.js',
        }
      },
      content: {
        options: {
          baseUrl: '<%= root %>app/scripts/content',
          out: '<%= root %>dist/scripts/content.js',
          mainConfigFile: '<%= root %>app/scripts/content/main.js',
        }
      },
      resources: {
        options: {
          baseUrl: '<%= root %>app/scripts/resources',
          out: '<%= root %>dist/scripts/resources.js',
          mainConfigFile: '<%= root %>app/scripts/resources/main.js',
        }
      }
    },

    uglify: {
      options: {
        mangle: false
      },
      my_target: {
        files: {
          '<%= root %>dist/vendor/requirejs/require.js': ['<%= root %>app/vendor/requirejs/require.js']
        }
      }
    },

    watch: {
      options: {
        nospawn: true
      },
      compass: {
        files: ['<%= root %>app/styles/{,*/}*.{scss,sass}'],
        tasks: ['compass:app', 'autoprefixer']
      }
    }

  });

  grunt.registerTask('test', [
    'jshint'
    //'mocha_phantomjs'
  ]);

  grunt.registerTask('default', [
    'clean:app',
    'test',
    'compass:app',
    'autoprefixer'
  ]);

  grunt.registerTask('build', [
    'test',
    'clean:dist',
    'imagemin',
    'compass:dist',
    'autoprefixer',
    'cssmin',
    'requirejs',
    'uglify',
    'copy'
  ]);

};
